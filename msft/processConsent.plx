#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <mandi.buswell@owmessaging.com>
#  Date:        July/2015
#  Version:     1.0.0 - July 11 2015
#
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use Getopt::Long;
use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;  
use com::owm::mosApi;
use com::owm::migBellCanada;
use Net::LDAP;
use XML::Simple;
use DateTime;
use Time::Local;
use POSIX qw(strftime);
use DBI;
use HTTP::Request::Common;
use HTTP::Response;
use com::owm::RetrievePassword;
use JSON;

# A package that provides an easy way to have a config file
use Config::Simple;

# A package that provides an easy way to launch multiple threads, with returns.
use Parallel::Fork::BossWorker; 

# A package that provides some status for our program
use Term::ProgressBar;

###########################################################################
# Global variables
###########################################################################

$| = 1;    ## Makes STDOUT flush immediately
my $SDEBUG;
my $SUCCESS_COUNT=0;
my $FAILED_COUNT=0;
my $SCRIPTNAME="processConsent";
my $PROCESSID=$$;
my $statsRecord = "active";
my $recordCount;

our $configPath = 'conf/migrator.conf';
our $logPath = 'conf/log.conf';

# Varables for progress bar
my $progress;
my $count      = 0;
my $startcount = 0;

# Logging object.
Log::Log4perl->init($logPath);
our $log = get_logger("PREP.PUID");
our $eventLog = get_logger("EVENT");

# Boss Working object.
my $bw;

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
  or $log->logdie("SCRIPT|FATAL: ".$cfg->error().". Please fix the issue and restart the migration program. Exiting.");

my $start = time;
#Create db connection
my $mysql_user =  $cfg->get_param("mysql_user");
my $mysql_pass = $cfg->get_param("mysql_pass");
my $mysql_db = $cfg->get_param("mysql_db");
my $mysql_host = $cfg->get_param("mysql_host");
# $log->debug("$email|$puid updateRefreshToken mysql_user=$mysql_user, mysql_pass=$mysql_pass, mysql_db=$mysql_db, mysql_host=$mysql_host");
my $table = "flow";
my $dbi_str = "DBI:mysql:" . $mysql_db . ";host=" . $mysql_host;
#my $dbhConsent = DBI->connect($dbi_str,$mysql_user,$mysql_pass) or return 1;



######################################################
# USAGE
######################################################
sub usage {

  print STDERR qq(

  Utility to apply microsoft account IDs or PUID to email entry in SUR.

  Command line to run script:

    preparePUID.plx -puidFile <puidFile> 
                                          [-d]

    where:
    <puidFile>     is the path to a file containing PUIDs of accounts to be migrated.

    Switches:
    -d              turn debug on.

);

  exit(0);
}

############################################################
## - Progress Init -                                       #
##   Initialize our progress bar based on addrs read in    #
############################################################
sub progress_init($) {
  my $size = shift;
  $count = 0;
  $progress = Term::ProgressBar->new( { count => $size, term_width => '100', ETA => 'linear',} );
  $progress->minor(0);
  $progress->max_update_rate(2); #dont update more than once per second
}

######################################################################
## - worker -                                                       ##
######################################################################
##MCB UDPATE
sub worker {
  my $work = shift;
  my $status;
  my $errorCode;
  my $dbmessage;
  my $dbstatus = "Success";

  # Declare variables needed for migBellCanada methods, but are not required by this script.

  my $consentstatus;
  # connect to DB in worker rather than in parent to avoid problems with open
  # DB connections across forks  
  my $dbhConsent = DBI->connect($dbi_str, $mysql_user, $mysql_pass) or return 'failed';  
  my $selectSth = $dbhConsent->prepare('SELECT refresh_token FROM '.$table.' WHERE email=?');  
  my $updateSth = $dbhConsent->prepare('UPDATE '.$table.' SET refresh_token=? WHERE email=?');  
  my $insertSth = $dbhConsent->prepare('INSERT INTO '.$table.' (refresh_token, email) VALUES (?, ?)');  
  
  # Input file is list of at least email address and PUIDs (one per line)
  # Accept input file with 2 or three columns
  my @workerArray = split(/\,/,$work->{data_line});
  my $email = $workerArray[0];
  my $puid = 'UNK999';
  if ( defined $workerArray[1] ) {
    $puid = $workerArray[1];
  }

  ##################################################################
  # General setup needed for each user
  ##################################################################

  $log->debug("$email|$puid: Start processing this entry [inPUID:$puid inEMAIL:$email] ...");

  if ( $email ) {
    my $password_endpoint = $cfg->param('password_endpoint');
    my $password_endpoint_file = $cfg->param('password_endpoint_file');
    my $password_retrieval_by_file = $cfg->param('password_retrieval_by_file');
    my $keyFile = $cfg->param('key_file');
    my $http_username = $cfg->param('http_username');
    my $http_password = $cfg->param('http_password');
    my ($pwd, $msftEmail, $pwdError) = ('','','1');
          
    if (defined $password_endpoint || defined $password_endpoint_file){
      if ($password_retrieval_by_file eq "true") {
        ($pwd,$pwdError) = com::owm::RetrievePassword::retrievePassword($password_endpoint_file,$email,$email,"OWMP",$log); #clear text stub version
        $log->debug("$email|$puid: Getting password via flat file");
      } else {
        ($pwd,$pwdError) = com::owm::RetrievePassword::retrievePassword($password_endpoint, $email,$email,"OWMP", $log, $keyFile);
        $log->debug("$email|$puid: Getting password via api");
        if ($pwdError eq 1) {
            #could not retrieve the password via the Telstra interface try the flat file
            if (defined $password_endpoint_file) {
                ($pwd, $pwdError) = com::owm::RetrievePassword::retrievePassword($password_endpoint_file,$email,$email,"OWMP",$log); 
                $log->debug("$email|$puid: API Failed, trying password via flat file");
            }
        }
      }
    } else {
      # Retrieve decrypted password from MSFT
      ($pwd,$msftEmail,$pwdError) = com::owm::MicrosoftPwd::getMicrosoftPwd($puid,$email);
      print "\ngetMicrosoftPwd\n" if ($SDEBUG);
    }

    if ($pwdError == 0) { #Indicates password was successfully retrieved.
      my ($refresh_token, $access_token) = getRefreshToken($puid,$email,$pwd,$cfg,$log);
      if ($refresh_token) {
        $log->debug("$email|$puid: refresh_token= $refresh_token ");
        $status = updateRefreshToken($dbhConsent,$puid,$email,$refresh_token,$access_token,$cfg,$log,$selectSth,$updateSth,$insertSth);
        $log->debug("$email|$puid: updated refresh token status = $status ");
        if ($status) {
          $log->error("$email|$puid: updateRefreshToken failed $status");
          $dbstatus="Failed";
          $dbmessage="Could not update refresh token. Returned $status. ";
        } else {
          $log->info("$email|$puid: updateRefreshToken succeed.");
        }
      } else {
        $log->error("$email|$puid: failed to get RefreshToken");
        $dbstatus="Failed";
        $dbmessage="Unable to retrieve refreshToken from Microsoft.";
      }
    } else {
      $log->error("$email|$puid: failed get password for $email");
      $dbstatus="Failed";
      $dbmessage="Unable to retrieve password from Telstra. Returned $pwdError ";
    }
  } else {
    $log->error("unknown|$puid: empty email address");
    $dbstatus="Failed";
    $dbmessage="No email address provided. ";
  }

  END:
  $dbhConsent->disconnect;
  undef($dbhConsent);
  $log->debug("$email|$puid: Stop processing [inPUID: $puid inEMAIL: $email ] ...");
  if ($dbstatus eq "Failed") {
    my $dbhEvent = com::owm::migBellCanada::connectEventDB($eventLog, $cfg, $log);
    if ($dbhEvent eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR)
    {
      $log->error('SCRIPT|Database error');
      return($dbstatus);
    }
    else
    {
      $log->debug('SCRIPT|Connected to event log database');
    }

    my $result=com::owm::migBellCanada::logEventsToDatabase($dbhEvent,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$com::owm::migBellCanada::DEFAULT_JOBID,$com::owm::migBellCanada::STATUS_FAILED_CONSENT,'ProcessConsent','','','',$dbstatus,$dbmessage);
    $dbhEvent->disconnect;
    undef($dbhEvent);
  }
  return($dbstatus);
}

######################################################################
# getRefreshToken
#
# Subroutine to add the PUID for an account.
#
# Args:
# - PUID of user whose account is being updated
# - email of user whose account is being updated
# - password of user whose account is being updated
# - configuration object
# - log object
#
######################################################################
sub getRefreshToken($$$$$)
{
  my $puid = shift;
  my $username = shift; 
  my $password = shift; 
  my $cfg = shift;
  my $log = shift;
  
  my $ms_access_token_url = $cfg->param('ms_access_token_url'); #"https://login.live.com/oauth20_token.srf"; #
  my $ms_access_client_id = $cfg->param('ms_access_client_id'); #"000000004413EF2A"; #
  my $ms_access_client_secret  = $cfg->param('ms_access_client_secret'); #"oi0mR0DYCSS7DDMHrFJ4C-Ojxpwfnh78"; #;#$


  my $grant_type = "password";
  my $code = "AUTHORIZATION_CODE";
  my $scope = "wl.offline_access wl.calendars wl.work_profile wl.basic wl.postal_addresses wl.phone_numbers wl.contacts_emails wl.contacts_birthday wl.imap wl.emails";

  # Create a POST for updating contact
  my $req = POST( $ms_access_token_url,
                  Content => [ 'client_id' => $ms_access_client_id,
                               'client_secret' => $ms_access_client_secret,
                               'username' => $username,
                 'password' => $password,
                               'code' => $code,
                               'grant_type' => 'password',
                               'scope' => $scope ] );
  $req->content_type('application/x-www-form-urlencoded');
  
  #$log->debug( "$username|$puid:".$req->as_string );

  # Do HTTP request.
  my $ua = LWP::UserAgent->new;
  my $response = HTTP::Response->new;
  $response = $ua->request($req);
  
  if ( $response->code == 200 ) {
    my $content = $response->content;
    my $decoded_json = decode_json($content);
    my $refresh_token = $decoded_json->{"refresh_token"};
    my $access_token = $decoded_json->{"access_token"};
    $log->debug("$username|$puid:response = ".$response->content);
    $log->debug("$username|$puid:access_token = $access_token");
    #return($access_token);
    return($refresh_token,$access_token);
  } else {
    $log->error("$username|$puid:Failed to get refresh. ".$response->code." - ".$response->message);
    #hide the password 
    my $thisReq = $req->as_string;
    my $codeIndex = index($thisReq,"&code");
    my $pwdIndex = index($thisReq,"password=");
    $pwdIndex += 9;
    $codeIndex = $codeIndex - $pwdIndex;
    substr($thisReq, $pwdIndex, $codeIndex) = "********";
    $log->debug("$username|$puid: request " . $thisReq);
    return(undef,undef);
  }
  
}

######################################################################
# getRefreshToken
#
# Subroutine to store the refresh/consetnt tokens for an account.
#
# Args:
# - database handle to the consent database
# - PUID of user whose account is being updated
# - email of user whose account is being updated
# - refresh token from MSFT
# - access token from MSFT
# - configuration object
# - log object
#
######################################################################
sub updateRefreshToken($$$$$$$$$$)
{
  my $dbhConsent = shift;
  my $puid = shift;
  my $email = shift;
  my $refresh_token = shift;
  my $access_token = shift;
  my $cfg = shift;
  my $log = shift;
  
  my $selectSth = shift;
  my $updateSth = shift;  
  my $insertSth = shift;

  my $status = 0;

  $selectSth->execute($email) or return 1;
  my $rowsReturnCount = $selectSth->rows;
  my @row = $selectSth->fetchrow_array();
  my $sth;

  if ($rowsReturnCount) {
    $log->info("$email|$puid:Account has no refresh_token, update it");
    $sth = $updateSth;
  } else {
    $log->info("$email|$puid:Account has no refresh_token, insert it");
    $sth = $insertSth;
  }

  $sth->execute($refresh_token, $email) or return 1;
  $sth->finish();
  $selectSth->finish();

  return $status;
}

######################################################################
## - Cleanup -                                                      ##
##   This routine is used to update the progress meter              ##
######################################################################
sub cleanup {
  $progress->update( ++$count );  # Update the counter
  my $result = shift;
  if (lc($result) eq 'success') {
    $SUCCESS_COUNT += 1;
  } elsif (lc($result) eq 'failed') {
    $FAILED_COUNT += 1;
  } # else {
  #   print Dumper(\$result);
  # }
}

###################################################################
## - BuildQueue -                                                 #
##   Get the email addrs, puid and populate the processing queue  #
###################################################################
sub buildQueue {
  my $in_file = shift;
  $log->debug("SCRIPT|Input file set to \'$in_file\'");

  print("Main processing progress...\n");

  open my $file, "<", $in_file
    or $log->logdie("SCRIPT|Unable to open file $in_file: $!");

  while ( my $data_line = <$file>) 
  {
    $startcount++;
    chomp($data_line);
    $bw->add_work( { data_line => $data_line } );
  }

  close($file);

  $log->debug("SCRIPT|Processing $startcount records read from input file.");
  print("Processing $startcount records.\n");

  return ($startcount);
}

######################################################
# ---------------------------------------------------
# Start Main
# ---------------------------------------------------
######################################################

$SDEBUG = $cfg->param('log_debug');

my $numThreads = $cfg->param('prepare_threads');
my $threadTimeout = $cfg->param('thread_timeout');
my $puidFile = '';

GetOptions( '-d',\$SDEBUG,               # Override debug in config file.
            '-puidFile=s',\$puidFile,
            ) || usage();

print("\n\n");

if ($SDEBUG) {
  print("Running in DEBUG mode\n");
  $log->level($DEBUG);               #Override debug in log file when -d or debug set in config file
}

# Check input file exists
if ( -e $puidFile ) {
  $log->info("SCRIPT|******BEGIN Processing PUID input file: $puidFile");
  $recordCount = `wc -l $puidFile`;
  chomp($recordCount);
} else {
  $log->error("SCRIPT|******BEGIN PUID input file is missing: $puidFile");
  usage();
} 

# Setup a boss to send out the work
$bw = Parallel::Fork::BossWorker->new(
  work_handler   => \&worker,       # Routine that does the work.
  result_handler => \&cleanup,      # Routine called after work is finish (optional).
  global_timeout => $threadTimeout, # In seconds.
  worker_count   => $numThreads );  # Number of worker threads.

# Build queue that boss will handout to worker threads.
my $rc = &buildQueue($puidFile);

# Initialize the progess counters.
&progress_init($rc);

# Set boss to start handing out work to threads.
$bw->process();

# Exit script gracefully.
my $time_taken = time - $start;

$progress->update($rc);

print "\n";    # This just makes things look clean
my $DBMESSAGE = 'Processed: '.$recordCount. ' Failed: '.$FAILED_COUNT .' Success: '.$SUCCESS_COUNT;

$log->info("SCRIPT|Completed took $time_taken seconds. $DBMESSAGE");

#Connect to event database

my $dbhEvent = com::owm::migBellCanada::connectEventDB($eventLog, $cfg, $log);
if ($dbhEvent eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR) {
  $log->error("SCRIPT|Database error exiting");
  exit 0; 
} else {
  $log->debug("SCRIPT|Connected to event log database");
} 

#still having mysql gone away errors with multiple threads
my $result=com::owm::migBellCanada::logEventsToDatabase($dbhEvent,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$com::owm::migBellCanada::DEFAULT_JOBID,$com::owm::migBellCanada::EVENT_PROCESS_CONSENT,'ProcessConsent','','','','Finished',$DBMESSAGE);

$dbhEvent->disconnect();
exit 0;
######################################################
# ---------------------------------------------------
# End Main
# ---------------------------------------------------
######################################################

######################################################
# ---------------------------------------------------
# BEGIN POD Documentation
# ---------------------------------------------------
######################################################

__END__



