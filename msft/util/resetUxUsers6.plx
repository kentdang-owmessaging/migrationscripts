#!/usr/bin/perl

###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     resetUxUsers.plx
#  Description: Script to erase and clear cache from UX CAL / PAB
#  Input:       See usage.
#  Author:      Andrew Perkins <andrew.perkins@owmessaging.com>
#  Date:        Sept 2015
#  Version:     1.0.0
#
#  Version History:
#
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
use lib "/opt/owm/imail/perl/lib";

use strict;
use warnings;
use Getopt::Long;
use Net::Telnet; 
use Net::LDAP;
use Net::LDAP::Constant (qw/LDAP_SUCCESS/);
use Config::Simple;
use DBI;

###########################################################################
# Global variables
###########################################################################

my $SDEBUG;
my $configPath = 'conf/migrator.conf';

my $cfg  = new Config::Simple();
$cfg->read($configPath)
  or die("SCRIPT|FATAL: ".$cfg->error().". Please fix the issue and restart the migration program. Exiting.");


###########################################################################
# usage
###########################################################################
sub usage {

        print STDERR qq(

  Utility to erase and clear cache for UX PAB and CAL.

  Command line: 

  # resetUxUsers.plx -email <email> [-d]
    where:
    <email>   is the user's email address that is to be erase / clear.

);

        exit(0);
}


#############################################################################
# Create database connection to the preload table and wipes any data for    #
# the user found in the table                                               #
#############################################################################
sub clear_preload_state($$)
{
    my ($email, $cfg) = @_;

    my ($localpart, $domain) = split(/@/, $email, 2);
    my $driver = $cfg->param('mysql_driver');
    my $database = $cfg->param('mysql_db1');
    my $host = $cfg->param('mysql_host');
    my $dsn = 'DBI:'.$driver.':database='.$database;
    my $userid = $cfg->param('mysql_user');
    my $password = $cfg->param('mysql_pass');
    
    my $dbh = DBI->connect($dsn, $userid, $password );
    if (! $dbh )
    {
        print 'ERROR: '.$email.':Could not connect to preload_state '.
            'database '.$DBI::errstr."\n\n";
        return(1);
    }

    my $sth = $dbh->prepare('DELETE FROM `preload_state` WHERE `domain`=?'.
                            ' AND `localpart`=?');
    unless ($sth->execute($domain, $localpart))
    {
        print 'ERROR: '.$email.':Could not wipe rows from preload_state '.
              'table: '.$DBI::errstr."\n\n";
        return(1);
    }

    print 'Deleted '.$sth->rows().' rows from preload_state table for user'.
          "\n\n";
    return(0);
}

#############################################################################################################
# lookupEmail
#
# Routine to lookup ldap data for account and return it.
#
# Args:
# - email
# - dir host
# - dir port
# - dir pwd
#
# Return:
# - status
# - mailboxstatus
# - migstatus
# - consentstatus
# - maillogin
#
##############################################################################################################

sub lookupEmail($$$$)
{
  my $email = shift;
  my $mx_dircache_host = shift;
  my $mx_dircache_port = shift;
  my $mx_dircache_pwd  = shift;

  my $ldap = Net::LDAP->new ( $mx_dircache_host, port => $mx_dircache_port );
  if (! $ldap) {
    die("Connect to $mx_dircache_host:$mx_dircache_port failed\n");
  }

  my $mesg = $ldap->bind ( "cn=root", password => $mx_dircache_pwd );
  if ( $mesg->code ne '0' ) {
    die("Bind to $mx_dircache_host:$mx_dircache_port failed. Check LDAP password.\n");
  }

  my $searchString = "mail=$email";
  print("searchString = $searchString\n") if ($SDEBUG);

  my $result = $ldap->search ( base   => "", filter  => "$searchString")
    || print("search failed\n");

  if ($result->code) {
    die("Email lookup for $email failed. $result->error\n");
  } elsif ($result->count == 0) {
    die("0 accounts had matching email ($email).\n");
  } elsif ($result->count > 1) {
    die("2 or more accounts had matching email ($email).\n");
  }

  my @entries = $result->entries;

  my $dn = $entries[0]->dn;
  my $mailboxstatus = $entries[0]->get_value("mailboxstatus");
  my $migstatus = $entries[0]->get_value("migstatus");
  my $consentstatus = $entries[0]->get_value("consentstatus");
  my $maillogin = $entries[0]->get_value("maillogin");
  my $userpabstorehost = $entries[0]->get_value("pabstorehost");
  my $usercalstorehost = $entries[0]->get_value("calendarstorehost");
  my $usercallocation = $entries[0]->get_value("calstoragelocation");
  my $userpablocation = $entries[0]->get_value("pabstoragelocation");

  return($dn,$mailboxstatus,$migstatus,$consentstatus,$maillogin,$userpabstorehost,$usercalstorehost,$userpablocation,$usercallocation);

}

###########################################################################
# deleteAccLdapAttributes
###########################################################################
sub deleteAccLdapAttributes($$$$)
{
  my $email = shift;
  my $mx_dircache_host = shift;
  my $mx_dircache_port = shift;
  my $mx_dircache_pwd  = shift;
  my $res = "";
  my ($user,$domain) = split(/@/,$email);

  my $ldap = Net::LDAP->new ( $mx_dircache_host, port => $mx_dircache_port );
  if (! $ldap) {
    die("Connect to $mx_dircache_host:$mx_dircache_port failed\n");
  }

  my $mesg = $ldap->bind ( "cn=root", password => $mx_dircache_pwd );
  if ( $mesg->code ne '0' ) {
    die("Bind to $mx_dircache_host:$mx_dircache_port failed. Check LDAP password.\n");
  }

  my $searchString = "mail=$email";
  print("searchString = $searchString\n") if ($SDEBUG);

  my $result = $ldap->search ( base   => "", filter  => "$searchString")
    || print("search failed\n");

  if ($result->code) {
    die("Email lookup for $email failed. $result->error\n");
  } elsif ($result->count == 0) {
    die("0 accounts had matching email ($email).\n");
  } elsif ($result->count > 1) {
    die("2 or more accounts had matching email ($email).\n");
  }

  my @entries = $result->entries;
  my $dn = $entries[0]->dn;

  #
  # Delete the PAB / CAL attributes for the account if they exist.
  #
  if ($entries[0]->exists('pabaddressbookdetails')){
	  $res = $ldap->modify( $dn, delete => [ 'pabaddressbookdetails' ] );
	  if ($res->code != LDAP_SUCCESS) {
		print("Delete pabaddressbookdetails failed : ".$res->error."\n");
		$res = "";
	  }
	}
  if ($entries[0]->exists('pabstoragelocation')){	
	  $res = $ldap->modify( $dn, delete => [ 'pabstoragelocation' ] );
	  if ($res->code != LDAP_SUCCESS) {
		print("Delete pabstoragelocation failed : ".$res->error."\n");
		$res = "";
	  }
  }
  if ($entries[0]->exists('pabstorehost')){
	  $res = $ldap->modify( $dn, delete => [ 'pabstorehost' ] );
	  if ($res->code != LDAP_SUCCESS) {
		print("Delete pabstorehost failed : ".$res->error."\n");
		$res = "";
	  }
  }
  if ($entries[0]->exists('calendarstorehost')){
	  $res = $ldap->modify( $dn, delete => [ 'calendarstorehost' ] );
	  if ($res->code != LDAP_SUCCESS) {
		print("Delete calendarstorehost failed : ".$res->error."\n");
		$res = "";
	  }
  }

  if ($entries[0]->exists('calstoragelocation')) {
	  $res = $ldap->modify( $dn, delete => [ 'calstoragelocation' ] );
	  if ($res->code != LDAP_SUCCESS) {
		print("Delete calstoragelocation failed : ".$res->error."\n");
		$res = "";
	  }
  }

  ############################
  # Remove migrated ldap entries
  #############################
  
  my @deleteArray;
  my @replaceArray;
  my @changesArray;
  
  if ($entries[0]->exists('migstatus')) {
	print("\n $email|Deleting migstatus ".$entries[0]->get_value("migstatus") );
	push @deleteArray, 'migstatus', $entries[0]->get_value("migstatus");
  }  
  if ($entries[0]->exists('netmaillocale')) {
	print("\n $email|Deleting netmaillocale ".$entries[0]->get_value("netmaillocale") );
	push @deleteArray, 'netmaillocale', $entries[0]->get_value("netmaillocale");
  } 
  if ($entries[0]->exists('msglocale')) {
	print("\n $email|Deleting msglocale ".$entries[0]->get_value("msglocale") );
	push @deleteArray, 'msglocale', $entries[0]->get_value("msglocale");
  }  
 
 if ($entries[0]->exists('msgtimezone')) {
	print("\n $email|Deleting msgtimezone ".$entries[0]->get_value("msgtimezone") );
	push @deleteArray, 'msgtimezone', $entries[0]->get_value("msgtimezone");
  }  
  
  if ($entries[0]->exists('mailautoreplymode')) {
	print("\n $email|Deleting mailautoreplymode ".$entries[0]->get_value("mailautoreplymode") );
	push @deleteArray, 'mailautoreplymode', $entries[0]->get_value("mailautoreplymode");
  }  
  
  if ($entries[0]->exists('calexternaldetails')) {
	print("\n $email|Deleting calexternaldetails ".$entries[0]->get_value("calexternaldetails") );
	push @deleteArray, 'calexternaldetails','';
  } 
  
  
# if ($entries[0]->exists('mailforwarding')) {
#	print("\n $email|Deleting mailforwarding ".$entries[0]->get_value("mailforwarding") );
#	push @deleteArray, 'mailforwarding', $entries[0]->get_value("mailforwarding");
#  }    
#  
 if ($entries[0]->exists('mailforwardingaddress')) {
	print("\n $email|Deleting mailforwardingaddress ".$entries[0]->get_value("mailforwardingaddress") );
	push @deleteArray, 'mailforwardingaddress','';
  }  

 
  print("\n $email|Replacing mailboxstatus P" );
  push @replaceArray, 'mailboxstatus', 'P';
  print("\n $email|Replacing cn $user" );
  push @replaceArray, 'cn', $user;
  print("\n $email|Replacing sn $user" );
  push @replaceArray, 'sn', $user;
  if ( $#deleteArray > 0 ) {
	push @changesArray, 'delete';
	push @changesArray, \@deleteArray;
 }
  if ( $#replaceArray > 0 ) {
   push @changesArray, 'replace';
   push @changesArray, \@replaceArray;
 }
 
 
  $result = $ldap->modify ( $dn, changes => [ @changesArray ] );
  print("\n\n $email|Updated. Result: " . $result->code . " ".$result->error."\n");
  print("\n************************************************ \n\n");
  if ($result->code) {
    print("\n $email|account not updated : ldap error code ".$result->code. " ".$result->error."\n");
    $ldap->unbind;
  }
  
 $ldap->unbind; 
}

###########################################################################
# unsetMx8Proxy
###########################################################################
sub unsetMx8Proxy($$)
{
  my $email = shift;
  my $cfg = shift;

  my $mx_dircache_host_src = $cfg->param('mx_dircache_host_src');
  my $mx_dircache_port_src = $cfg->param('mx_dircache_port_src');
  my $mx_dircache_pwd_src  = $cfg->param('mx_dircache_pwd_src');
  my $res = "";
  my ($user, $domain) = split(/@/, $email);

  my $ldap = Net::LDAP->new($mx_dircache_host_src, port => $mx_dircache_port_src);
  unless ($ldap)
  {
    die('Connect to MX8 '.$mx_dircache_host_src.':'.$mx_dircache_port_src .'failed');
  }

  my $mesg = $ldap->bind ('cn=root', password => $mx_dircache_pwd_src);
  if ($mesg->code != LDAP_SUCCESS)
  {
    die('Bind to MX8 '.$mx_dircache_host_src.':'.$mx_dircache_port_src.' failed. Check LDAP password.');
  }

  my $searchString = 'mail='.$email;
  print("mx8 searchString = $searchString\n") if ($SDEBUG);

  my $result = $ldap->search (base  => "", filter => $searchString);

  if ($result->code != LDAP_SUCCESS)
  {
    die("Email lookup for $email failed in MX8. $result->error\n");
  }
  elsif ($result->count == 0)
  {
    print 'No accounts matched in MX8, assuming a Windows Live account - skipping trying to undo proxy settings'."\n";
    return;
  }
  elsif ($result->count > 1)
  {
    die('2 or more accounts had matching email ('.$email.') in MX8');
  }

  my @entries = $result->entries;
  my $entry = $entries[0];
  my $changed = 0;
  if ($entry->exists('mailboxstatus'))
  {
      my $mailboxstatus = $entry->get_value('mailboxstatus');
      if (lc($mailboxstatus) ne 'a')
      {
          $entry->replace('mailboxstatus' => 'A');
          print 'Replacing mailboxstatus value of '.$mailboxstatus.'" with a '.
            'value "A" in MX8'."\n";
          $changed = 1;
      }
  }
  else
  {
      $entry->add('mailboxstatus' => 'A');
      print 'Adding "mailboxstatus=A" to MX8'."\n";
      $changed = 1;
  }

  if ($entry->exists('mailsmtprelayhost'))
  {
      print 'Deleting mailsmtprelayhost from MX8'."\n";
      $entry->delete('mailsmtprelayhost');
      $changed = 1;
  }

  if ($entry->exists('mailimapproxyhost'))
  {
      print 'Deleting mailimapproxyhost from MX8'."\n";
      $entry->delete('mailimapproxyhost');
      $changed = 1;
  }

  if ($entry->exists('mailpopproxyhost'))
  {
      print 'Deleting mailpopproxyhost from MX8'."\n";
      $entry->delete('mailpopproxyhost');
      $changed = 1;
  }

  if ($changed)
  {
    $mesg = $entry->update($ldap);

    if ($result->code != LDAP_SUCCESS)
    {
      die('Cannot remove proxy settings from MX8: '.$result->error);
    }
    print 'MX8 updated to remove any proxy settings.'."\n";
  }
  else
  {
    print 'No proxy settings in MX8 to remove - MX8 not changed'."\n";
  }
}
###########################################################################
# resetUxUser
###########################################################################
sub resetUxUser($$$)
{
  my $host = shift;
  my $port = shift;
  my $email = shift;
  my $result = "";

  (my $user, my $domain) = split(/\@/,$email);

  #
  # Telnet to host / port.
  #
  my $remote = new Net::Telnet (Timeout => 5, Errmode => 'return');

  $remote->port($port); 
  $remote->open($host); 

  #
  # Issue commands to erase and clear cache.
  #

  $remote->waitfor('/>$/i');

  my $cmd = "login p write";
  $remote->print($cmd);
  $remote->waitfor('/OK$/i') or print("Expected return for '$cmd' of 'OK' was not received.\n");

  $cmd = "cache clear user $email";
  $remote->print($cmd);
  $remote->waitfor('/OK$/i') or $result .= "Expected return for '$cmd' of 'OK' was not received.\n";
  
  if ($port eq "5230" ) {
	$cmd = "cache clear calendar $email";
	$remote->print($cmd);
	$remote->waitfor('/OK$/i') or $result .= "Expected return for '$cmd' of 'OK' was not received.\n";
  }
  
  $cmd = "user $domain delete $user erase";
  $remote->print($cmd);
  $remote->waitfor('/OK$/i') or $result .= "Expected return for '$cmd' of 'OK' was not received.\n";

  #
  # If successful, please out the following...
  #
  if ($result eq "") {
    print("\n $email successfully erased from $host:$port.\n");
  } else {
    print($result);
  }

}


###########################################################################
# MAIN
###########################################################################
my $email;
my $calHost;
my $pabHost;
my $calLoc;
my $pabLoc;
my $dn;
my $junk;
my $ldapHost = $cfg->param('mx_dirserv_host');
my $ldapPort = $cfg->param('mx_dirserv_port');
my $ldapPwd = $cfg->param('mx_dirserv_pwd');

#
# Parse command line.
#
GetOptions( '-d',\$SDEBUG,               # Override debug in config file.
            'email=s',\$email,
            ) || usage();

if ( (! defined $email) || ($email eq '') ) {
  usage();
}

#
# Get CAL and PAB data for user from LDAP
#
($dn,$junk,$junk,$junk,$junk,$pabHost,$calHost,$pabLoc,$calLoc) = lookupEmail($email,$ldapHost,$ldapPort,$ldapPwd);


if ( $SDEBUG ) {
  print("DEBUG: email = $email\n");
  if (defined($pabHost))
  {
    print("DEBUG: pabHost   = $pabHost\n");
  }
  else
  {
    print("DEBUG: pabHost   =\n");
  }
  if (defined($pabLoc))
  {
    print("DEBUG: pabLoc    = $pabLoc\n");
  }
  else
  {
    print("DEBUG: pabLoc    =\n");
  }
  if (defined($calHost))
  {
    print("DEBUG: calHost   = $calHost\n");
  }
  else
  {
    print("DEBUG: calHost   =\n");
  }
  if (defined($calLoc))
  {
    print("DEBUG: calLoc    = $calLoc\n");
  }
  else
  {
    print("DEBUG: calLoc    =\n");
  }
}

if (defined($pabHost))
{
  resetUxUser($pabHost,'4211',$email);
}
if (defined($calHost))
{
  resetUxUser($calHost,'5230',$email);
}

deleteAccLdapAttributes($email,$ldapHost,$ldapPort,$ldapPwd);

clear_preload_state($email, $cfg);

# should this be done first?
unsetMx8Proxy($email, $cfg);
