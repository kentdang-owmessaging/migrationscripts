#!/bin/bash

# Utility to take input file (list of PUIDs, one per line) and reset those PUID's mailboxstatus to 'P'

for PUID in `cat $1`
do

DN=`ldapsearch -h mtlmig02 -p 389 -D cn=root -w secret puid=$PUID dn`

echo "dn: $DN
changetype: modify
replace: mailboxstatus
mailboxstatus: P" | \
ldapmodify -h tordir01 -p 5005 -D "cn=root" -w secret

done
