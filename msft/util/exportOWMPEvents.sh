#!/bin/bash
#
# exportOWMPEvents.sh, Telstra, d363756
#
# History:
#   29.11.2015, d363756: First draft
#
# Purpose:
#  Export OWMP migration.events table into a file for Migration Support Solution (MSS) 
#  Export file is processed in SDR, and data loaded to Migration Support Database (MSD)
#
# Prerequisite:
#  Valid user has to exit to execute SQL statement on the migration.events table
#    mysql> CREATE USER 'exportevents4mss'@'localhost' IDENTIFIED BY 'ndxjWD5X@jx_Kc';
#    mysql> GRANT SELECT ON migration.events TO 'exportevents4mss'@'localhost';
#
# Use:
#  exportOWMPEvents.sh [JobID]
#
# Files:
#  exportOWMPEvents.timestamp
# 
# Output:
#  stdout: Console messages (errors and results)
#  OWMPEvents-<TIMESTAMP>.dat.gz: Export file
#    TIMESTAMP: YYYYDDMMHHmmSS
#    File iz compressed (gz)
#    Example filename: OWMPEvents-20150814114732.dat.gz
#
#  OWMPEvents-<TIMESTAMP>.dat file structure:
#       <date_time>|<milliseconds>|<migration_host>|<migration_scriptname>|<line_number>|<event_id>|<email_address>|<migration_job_id>|<event_details>
#   Fields:
#     1. <date_time>: Event date and time in yyyy-MM-dd hh:mm:ss format
#     2. <milliseconds>: Milliseconds from the date_time field if avaialble
#     3. <migration_host>: Hostname of the migration server that has produced the event
#     4. <migration_scriptname>: Title of the script that has produced the event
#     5. <line_number>: Line number within the file where the log statement was issued
#     6. <event_id>: Event Identifier. Links to Events table that offers details
#     7. <email_address>: Email address of the account that is being migrated
#     8. <migration_job_id>: Unique Identifier of a migration job 
#     9. <event_details>: Details of the event. Large text field
#
#  Sample record:
#    2015-10-14 14:46:53|0|nsdu2mig01t.dlx1.msg.in.telstra.com.au|migrateBatch|310|100|fn1_poc_10895@uat.bigpond.com|001|event_name=MigrationStart, migtype=WL, secondary_id=00034001A9516CB5, status=, message="Start migration"
#
# Example:
#       ./exportOWMPEvents.sh 123
#
MAILX=/bin/mailx
MYSQLCLI=/usr/bin/mysql
SED=/bin/sed
GZIP=/bin/gzip
DBUSER=exportevents4mss
DBPASSWORD="ndxjWD5X@jx_Kc"
DBSERVER=nsdu2mig01t.dlx1.msg.in.telstra.com.au
#
# mysql -uexportevents4mss -pndxjWD5X@jx_Kc --batch --skip-column-names < exportOWMPEvents_20151129.sql | sed "s/\t/\|/g" | head
#
MYSQLCMDPARAMS="--batch --skip-column-names"
#
# Output filename is OWMPEvents-<TIMESTAMP>.dat.gz, where TIMESTAMP format is "YYYYDDMMHHmmSS" and the file si compressed (gz)
# Posiblity that simultaneous runs of the script may result in the output file of the same name was considered, and that 
#   should not be a problem, as long as the file is appended rather than overwritten. However, a simple test is done
#   to tray to avoid such a situation, and try to write to a file exlusively in any run of the script.
OUTDIR="/opt/owm/migration/eventReports/"
OUTFILE="${OUTDIR}/OWMPEvents-`date +%Y%m%d%H%M%S`.dat"
if [ ! -e "${OUTFILE}" ] ; then
  touch "$OUTFILE"
else
  # $RANDOM returns a different random integer at each invocation.
  # Nominal range: 0 - 32767 (signed 16-bit integer)
  # Create another filename after a random delay of up to 3 seconds
  sleep $[($RANDOM % 3)].$[($RANDOM % 1000)+1]s
  OUTFILE="${OUTDIR}/OWMPEvents-`date +%Y%m%d%H%M%S`.dat"
  touch "$OUTFILE"
fi

TMPFILE="/tmp/`basename $0 .sh`_`date +%Y%m%d`.$$"
UXLOGIN="`whoami`"
UXNAME="`getent passwd ${UXLOGIN} | cut -d ':' -f 5`"
UXHOST="`hostname`"
DATE=`date '+%Y-%m-%d'`
#
#MAILFROM="${UXNAME} <${UXLOGIN}@${UXHOST}>"
MAILFROM="exportOWMPEvents <${UXLOGIN}@${UXHOST}>"
REPLYTO="milan.ristic@team.telstra.com"
SUBJECT="Migration event log file exported"
MAILTO="milan.ristic@team.telstra.com"
#BCC="dABCDEF@team.telstra.com"
BCC="milan.ristic@team.telstra.com"
MAILHEADERS="$(echo -e "${SUBJECT}\nFrom: ${MAILFROM}\nReply-to: ${REPLYTO}\nContent-Type: text/html\n")"

if [[ $1 = "" ]]
then
  echo "Syntax: `basename $0` [<JobID>|TODAY]"
  echo "Examples:"
  echo "  `basename $0` TODAY"
  echo "  `basename $0` 1289"
  exit 33
else
  if [[ $1 = "TODAY" ]]
  then
    FROMDATE=`date '+%Y-%m-%d'`
    SQLFILTER="create_time>'${FROMDATE}'"
        LOGMESSAGE="`date`,START,Exporting event logs for today, ${FROMDATE}"
    echo "${LOGMESSAGE}"
  else
    SQLFILTER="migration_job_id=$1 AND primary_id IS NOT NULL AND primary_id != '' AND event_id IN(100,101,102,103,115,117,162,185,190,197,198,199)"
    LOGMESSAGE="`date`,START,Exporting event logs for JobID=$1"
    echo "${LOGMESSAGE}"
  fi
fi

# Make sure the temporary file is clean
echo "" > ${TMPFILE}
date >> ${TMPFILE}
echo "Exported migration event log where ${SQLFILTER}" >> ${TMPFILE}

#
# Extract event logs
#
echo "
use migration;
SELECT
  create_time as date_time,
  CAST(MICROSECOND('create_time')/1000 AS UNSIGNED) AS milliseconds,
  migration_host,
  migration_scriptname,
  line_number,
  event_id,
  primary_id AS email_address,
  migration_job_id,
--  message
  CONCAT('event_name=', event_name, ', migtype=', migtype, ', secondary_id=', secondary_id, 
                 ', status=', status, ', message=\"', message, '\"') AS event_details
FROM
  migration.events
WHERE
  ${SQLFILTER}
;
QUIT
" |  mysql --user=$DBUSER --password=$DBPASSWORD --host=$DBSERVER ${MYSQLCMDPARAMS} | ${SED} "s/\t/\|/g" >> ${OUTFILE}

echo "`wc -l ${OUTFILE}`" >> ${TMPFILE}

echo "`date`,MAIL,Sending email report with subject \"${SUBJECT}\" to ${MAILTO}, bcc to: ${BCC}"
#${MAILX} -s "${MAILHEADERS}" "${MAILTO}" < ${TMPFILE}
${MAILX} -s "${MAILHEADERS}" "${MAILTO}" -b "${BCC}" < ${TMPFILE}

rm -f ${TMPFILE}

echo "`date`,FINISH,Done"

