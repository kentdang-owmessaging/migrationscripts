#!/usr/bin/perl --
###Release Version: MIGTK-v1.3-0304
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     resetUxUsers.plx
#  Description: Script to erase and clear cache from UX CAL / PAB
#  Input:       See usage.
#  Author:      Andrew Perkins <andrew.perkins@owmessaging.com>
#  Date:        Sept 2015
#  Version:     1.0.0
#
#  Version History:
#
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                        # Locate this script
use lib "$FindBin::Bin/lib";        # Tell script where to find libraries
use lib "$FindBin::Bin/migperl";    # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use Getopt::Long;
use Net::Telnet;
use Net::LDAP;
use Net::LDAP::Constant (qw/LDAP_SUCCESS/);
use Config::Simple;
use DBI;
use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;

use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use com::owm::mosApi;
use com::owm::migBellCanada;

###########################################################################
# Global variables
###########################################################################

my $SDEBUG;
my $configPath = 'conf/migrator.conf';
my $configLog  = 'conf/log.conf';

my $cfg = new Config::Simple();
$cfg->read($configPath)
  or die( "SCRIPT|FATAL: " . $cfg->error() . ". Please fix the issue and restart the migration program. Exiting." );

Log::Log4perl->init($configLog);
my $log = get_logger("MIGRATE");

my $skipMSS;

###########################################################################
# usage
###########################################################################
sub usage {

    print STDERR qq(

  Utility to erase and clear cache for UX PAB and CAL.

  Command line: 

  # resetUxUsers.plx -email <email> [-d] [-state p|i] [-mode WL|MYI|MX8]
    where:
    <email>   is the user's email address that is to be erase / clear.
	-d 		  debug
	-state	  p for preload or i for initial (default is p)
	-mode     WL for microsoft, MYI for myInbox user or MX8 for MX8 users.  Default is WL.

	);
    exit(0);
}

#############################################################################
# Create database connection to the preload table and wipes any data for    #
# the user found in the table                                               #
#############################################################################
sub clear_preload_state($$) {
    my ( $email, $cfg ) = @_;

    my ( $localpart, $domain ) = split( /@/, $email, 2 );
    my $driver   = $cfg->param('mysql_driver');
    my $database = $cfg->param('mysql_db1');
    my $host     = $cfg->param('mysql_host');
    my $dsn      = 'DBI:' . $driver . ':database=' . $database . ':host=' . $host;
    my $userid   = $cfg->param('mysql_user');
    my $password = $cfg->param('mysql_pass');

    my $dbh = DBI->connect( $dsn, $userid, $password );
    if ( !$dbh ) {
        print 'ERROR: ' . $email . ':Could not connect to preload_state ' . 'database ' . $DBI::errstr . "\n\n";
        return (1);
    }

    my $sth = $dbh->prepare( 'DELETE FROM `preload_state` WHERE `domain`=?' . ' AND `localpart`=?' );
    unless ( $sth->execute( $domain, $localpart ) ) {
        print 'ERROR: ' . $email . ':Could not wipe rows from preload_state ' . 'table: ' . $DBI::errstr . "\n\n";
        return (1);
    }

    print 'Deleted ' . $sth->rows() . ' rows from preload_state table for user' . "\n\n";
    return (0);
}

#############################################################################################################
# lookupEmail
#
# Routine to lookup ldap data for account and return it.
#
# Args:
# - email
# - dir host
# - dir port
# - dir pwd
#
# Return:
# - status
# - mailboxstatus
# - migstatus
# - consentstatus
# - maillogin
#
##############################################################################################################

sub lookupEmail {
    my $email  = shift;
    my $result = shift;
    my $ldap   = shift;

    my @entries = $result->entries;

    my $dn               = $entries[0]->dn;
    my $mailboxstatus    = $entries[0]->get_value("mailboxstatus");
    my $migstatus        = $entries[0]->get_value("migstatus");
    my $consentstatus    = $entries[0]->get_value("consentstatus");
    my $maillogin        = $entries[0]->get_value("maillogin");
    my $userpabstorehost = $entries[0]->get_value("pabstorehost");
    my $usercalstorehost = $entries[0]->get_value("calendarstorehost");
    my $usercallocation  = $entries[0]->get_value("calstoragelocation");
    my $userpablocation  = $entries[0]->get_value("pabstoragelocation");

    return (
        $dn,               $mailboxstatus,    $migstatus,       $consentstatus, $maillogin,
        $userpabstorehost, $usercalstorehost, $userpablocation, $usercallocation
    );

}

sub setMailboxStatus {
    my $email  = shift;
    my $cfg    = shift;
    my $status = shift;
    my $result = shift;
    my $ldap   = shift;

    my @entries = $result->entries;
    my $dn      = $entries[0]->dn;

    my @replaceArray;
    my @changesArray;
    push @replaceArray, 'mailboxstatus', $status;

    if ( $#replaceArray > 0 ) {
        push @changesArray, 'replace';
        push @changesArray, \@replaceArray;
    }
    $result = $ldap->modify( $dn, changes => [@changesArray] );
    print( "\n\n $email|Updated. Result: " . $result->code . " " . $result->error . "\n" );
    print("\n************************************************ \n\n");
    if ( $result->code ) {
        print( "\n $email|account not updated : ldap error code " . $result->code . " " . $result->error . "\n" );
        $ldap->unbind;
        return ("Failure");
    }

    return ("Success");
}

sub connectLDAP {
    my $email            = shift;
    my $cfg              = shift;
    my $mx_dircache_host = $cfg->param('mx_dircache_host');
    my $mx_dircache_port = $cfg->param('mx_dircache_port');
    my $mx_dircache_pwd  = $cfg->param('mx_dircache_pwd');

    my $ldap = Net::LDAP->new( $mx_dircache_host, port => $mx_dircache_port );
    if ( !$ldap ) {
        die("Connect to $mx_dircache_host:$mx_dircache_port failed\n");
    }

    my $mesg = $ldap->bind( "cn=root", password => $mx_dircache_pwd );
    if ( $mesg->code ne '0' ) {
        die("Bind to $mx_dircache_host:$mx_dircache_port failed. Check LDAP password.\n");
    }

    my $searchString = "mail=$email";
    print("searchString = $searchString\n") if ($SDEBUG);

    my $result = $ldap->search( base => "", filter => "$searchString" )
      || print("search failed\n");

    if ( $result->code ) {
        die("Email lookup for $email failed. $result->error\n");
    } elsif ( $result->count == 0 ) {
        die("0 accounts had matching email ($email).\n");
    } elsif ( $result->count > 1 ) {
        die("2 or more accounts had matching email ($email).\n");
    }

    return ( $result, $ldap );

}
###########################################################################
# deleteAccLdapAttributes
# For PRELOAD reset delete Addressbook, signature
# Fro INITIAL reset delete all.
###########################################################################
sub deleteAccLdapAttributes {
    my $email = shift;
    my $cfg   = shift;
    my $state = shift;
    my $mode  = shift;
    my $res   = "";
    my ( $user, $domain ) = split( /@/, $email );

    my ( $result, $ldap ) = connectLDAP( $email, $cfg );

    my @entries = $result->entries;
    my $dn      = $entries[0]->dn;

    #
    # Delete the PAB / CAL attributes for the account if they exist.
    #
    if ( $state eq 'i' ) {
        #For preload runs, addressbook is delete as part of the migrateBatch process
        if ( $entries[0]->exists('pabaddressbookdetails') ) {
            $res = $ldap->modify( $dn, delete => ['pabaddressbookdetails'] );
            if ( $res->code != LDAP_SUCCESS ) {
                print( "Delete pabaddressbookdetails failed : " . $res->error . "\n" );
                $res = "";
            }
        }
        if ( $entries[0]->exists('pabstoragelocation') ) {
            $res = $ldap->modify( $dn, delete => ['pabstoragelocation'] );
            if ( $res->code != LDAP_SUCCESS ) {
                print( "Delete pabstoragelocation failed : " . $res->error . "\n" );
                $res = "";
            }
        }
        if ( $entries[0]->exists('pabstorehost') ) {
            $res = $ldap->modify( $dn, delete => ['pabstorehost'] );
            if ( $res->code != LDAP_SUCCESS ) {
                print( "Delete pabstorehost failed : " . $res->error . "\n" );
                $res = "";
            }
        }
        if ( $entries[0]->exists('calendarstorehost') ) {
            $res = $ldap->modify( $dn, delete => ['calendarstorehost'] );
            if ( $res->code != LDAP_SUCCESS ) {
                print( "Delete calendarstorehost failed : " . $res->error . "\n" );
                $res = "";
            }
        }
        if ( $entries[0]->exists('calaccesscontrolentry') ) {
            $res = $ldap->modify( $dn, delete => ['calaccesscontrolentry'] );
            if ( $res->code != LDAP_SUCCESS ) {
                print( "Delete calaccesscontrolentry failed : " . $res->error . "\n" );
                $res = "";
            }
        }
        if ( $entries[0]->exists('calstoragelocation') ) {
            $res = $ldap->modify( $dn, delete => ['calstoragelocation'] );
            if ( $res->code != LDAP_SUCCESS ) {
                print( "Delete calstoragelocation failed : " . $res->error . "\n" );
                $res = "";
            }
        }
    }
    ############################
    # Remove migrated ldap entries
    #############################

    my @deleteArray;
    my @replaceArray;
    my @changesArray;

    print "\nThe following LDAP values will be modified: \n";
    if ( $entries[0]->exists('migstatus') && ( $state eq 'i' ) ) {
        print( "\n $email|Deleting migstatus " . $entries[0]->get_value("migstatus") );
        push @deleteArray, 'migstatus', $entries[0]->get_value("migstatus");
    }

    if ( $entries[0]->exists('netmaillocale') ) {
        print( "\n $email|Deleting netmaillocale " . $entries[0]->get_value("netmaillocale") );
        push @deleteArray, 'netmaillocale', $entries[0]->get_value("netmaillocale");
    }

    if ( $entries[0]->exists('msglocale') ) {
        print( "\n $email|Deleting msglocale " . $entries[0]->get_value("msglocale") );
        push @deleteArray, 'msglocale', $entries[0]->get_value("msglocale");
    }

    if ( $entries[0]->exists('msgtimezone') ) {
        print( "\n $email|Deleting msgtimezone " . $entries[0]->get_value("msgtimezone") );
        push @deleteArray, 'msgtimezone', $entries[0]->get_value("msgtimezone");
    }

    if ( $entries[0]->exists('mailautoreplymode') && $mode eq 'MX8' ) {
        print( "\n $email|Deleting mailautoreplymode " . $entries[0]->get_value("mailautoreplymode") );
        push @deleteArray, 'mailautoreplymode', $entries[0]->get_value("mailautoreplymode");
    }

    if ( $entries[0]->exists('mailBlockedSendersList') && $mode eq 'WL' ) {
        print( "\n $email|Deleting mailBlockedSendersList " . $entries[0]->get_value("mailBlockedSendersList") );
        push @deleteArray, 'mailBlockedSendersList', '';
    }

    if ( $entries[0]->exists('mailApprovedSendersList') && $mode eq 'WL' ) {
        print( "\n $email|Deleting mailApprovedSendersList " . $entries[0]->get_value("mailApprovedSendersList") );
        push @deleteArray, 'mailApprovedSendersList', '';
    }

    #Subscribed calendars are remigrated each time even for preload
    if ( $entries[0]->exists('calexternaldetails') ) {
        print( "\n $email|Deleting calexternaldetails " . $entries[0]->get_value("calexternaldetails") );
        push @deleteArray, 'calexternaldetails', '';
    }

    if ( $entries[0]->exists('mailforwardingaddress') && $mode eq 'WL' ) {
        #only delete mail forwards for microsoft customers
        print("\n $email|Deleting mailforwarding ");
        push @deleteArray,  'mailforwardingaddress', '';
        push @deleteArray,  'maildeliveryoption',    '';
        push @replaceArray, 'mailforwarding',        '0';
    }

    print("\n $email|Replacing mailboxstatus P");
    push @replaceArray, 'mailboxstatus', 'P';

    print("\n $email|Replacing cn $user");
    push @replaceArray, 'cn', $user;
    print("\n $email|Replacing sn $user");
    push @replaceArray, 'sn', $user;

    if ( $#deleteArray > 0 ) {
        push @changesArray, 'delete';
        push @changesArray, \@deleteArray;
    }
    if ( $#replaceArray > 0 ) {
        push @changesArray, 'replace';
        push @changesArray, \@replaceArray;
    }

    $result = $ldap->modify( $dn, changes => [@changesArray] );
    print( "\n\n$email|Updated. Result: " . $result->code . " " . $result->error . "\n" );
    if ( $result->code ) {
        print( "\n $email|account not updated : ldap error code " . $result->code . " " . $result->error . "\n" );
        $ldap->unbind;
    }

    $ldap->unbind;
}

###########################################################################
# unsetMx8Proxy
###########################################################################
sub unsetMx8Proxy($$$) {
    my $email = shift;
    my $cfg   = shift;
    my $log   = shift;

    my $mx_dircache_host_src     = $cfg->param('mx_dircache_host_src');
    my $mx_dircache_port_src     = $cfg->param('mx_dircache_port_src');
    my $mx_dircache_pwd_src      = $cfg->param('mx_dircache_pwd_src');
    my %attributeMap             = (
        'mailsmtprelayhost' => $cfg->param('mx8_proxy_user_host_smtp'),
        'mailimapproxyhost' => $cfg->param('mx8_proxy_user_host_imap'),
        'mailpopproxyhost' => $cfg->param('mx8_proxy_user_host_pop'),
    );

    my ( $user, $domain ) = split( /@/, $email );

    my $ldap = Net::LDAP->new( $mx_dircache_host_src, port => $mx_dircache_port_src );
    unless ($ldap) {
        die( 'Connect to MX8 ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src . 'failed' );
    }

    my $mesg = $ldap->bind( 'cn=root', password => $mx_dircache_pwd_src );
    if ( $mesg->code != LDAP_SUCCESS ) {
        die( 'Bind to MX8 ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src . ' failed. Check LDAP password.' );
    }

    my $searchString = 'mail=' . $email;
    print("mx8 searchString = $searchString\n") if ($SDEBUG);

    my $result = $ldap->search( base => "", filter => $searchString );

    if ( $result->code != LDAP_SUCCESS ) {
        die("Email lookup for $email failed in MX8. $result->error\n");
    } elsif ( $result->count == 0 ) {
        print 'No accounts matched in MX8, skipping trying to undo proxy settings' . "\n";
        return;
    } elsif ( $result->count > 1 ) {
        die( '2 or more accounts had matching email (' . $email . ') in MX8' );
    }

    my @entries = $result->entries;
    my $entry   = $entries[0];
    my $changed = 0;
    if ( $entry->exists('mailboxstatus') ) {
        my @mailboxstatus = $entry->get_value('mailboxstatus');
        if (scalar(@mailboxstatus) > 1) {
            print 'User has multiple values for the mailboxstatus attribute '.
                  'in MX8, not performing further checks'."\n";
            $log->error($email.'|User has multiple values for the '.
                        'mailboxstatus attribute in MX8, not performing '.
                        'further checks');
            return;
        }
        my $mailboxstatus = $mailboxstatus[0];
        if ( lc($mailboxstatus) eq 'p' ) {
            $entry->replace( 'mailboxstatus' => 'A' );
            print 'Replacing mailboxstatus value of "' . $mailboxstatus . 
                  '" with a value "A" in MX8 (pending other checks)' . "\n";
            $log->info($email.'|Replacing mailboxstatus value of "' . 
                       $mailboxstatus . '" with a value "A" in MX8 (pending '.
                       ' other checks)');
            $changed = 1;

            # loop over the other attributes and make sure they are
            # the expected value, else skip changing mailboxstatus
            foreach my $attribute (sort(keys(%attributeMap))) {
                # only need to do the check if $changed is still true
                if ($changed) {
                    if ( $entry->exists('attribute') ) {
                        my @attr = $entry->get_value('attribute');
                        # more than one value?  problem!
                        if (scalar(@attr) > 1) {
                            print 'User has multiple values for the "'.$attribute.'" '.
                                  'attribute, abandoning previous changes for this user.'."\n";
                            $log->error($email.'|User has multiple values for the  "'.
                                  $attribute.'" attribute, abandoning previous changes.');
                            $changed = 0;
                        } elsif (scalar(@attr) == 1) {
                            my $expectedValue = $attributeMap{$attribute};
                            my $attr = $attr[0];
                            if (lc($attr) eq $expectedValue) {
                                print 'Deleting "'.$attribute.'" from MX8' . "\n";
                                $log->info($email.'|Deleting "'.$attribute.'" value of "'.$expectedValue.'" from MX8');
                                $entry->delete($attribute => $expectedValue);
                                $changed = 1;
                            } else {
                                print 'User has "'.$attribute.'" setting of "'.$attr.
                                      '", which is different to the configured value of "'.
                                      $expectedValue.'", abandoning '.
                                      'previous change(s) for this user'."\n";
                                $log->error($email.'|User has "'.$attribute.'" setting of "'.
                                      $attr.'", which is different to the '.
                                      'configured value of "'.$expectedValue.
                                      '", abandoning previous change(s)');
                                $changed = 0;
                            }
                        } else {
                            # is it even possible for an attibute to exist
                            # with no values?
                            print 'Unknown situation - "'.$attribute.'" attribute '.
                                  'with no values, abandoning previous change(s) for this user'."\n";
                            $log->error($email.'|Unknown situation - "'.$attribute.'" attribute '.
                                  'with no values, abandoning previous change(s)');
                            $changed = 0;
                        }
                    } else {
                        print 'User does not have a "'.$attribute.'" attribute, '.
                              'abandoning previous change(s)';
                        $log->error($email.'|User does not have a "'.$attribute.'" attribute, '.
                              'abandoning previous change(s)');
                        $changed = 0;
                    }
                }
            }
        } else {
            print 'User has a mailbox status of "'.$mailboxstatus.'" in MX8, '.
                  'not touching user'."\n";
            $log->info($email.'|User has a mailbox status of "'.$mailboxstatus.
                  '" in MX8, '.'not touching user');
            $changed = 0;
        }
    } else {
        print 'User has no mailboxstatus attribute, not touching user' . "\n";
        $log->info($email.'|User has no mailbox status attribute in MX8, not '.
                   'touching user');
        $changed = 0;
    }

    if ($changed) {
        $mesg = $entry->update($ldap);

        if ( $result->code != LDAP_SUCCESS ) {
            $log->error('Cannot remove proxy settings from MX8: ' . $result->error);
            die( 'Cannot remove proxy settings from MX8: ' . $result->error );
        }

        print 'MX8 updated to remove proxy settings.' . "\n";
        $log->info($email.'|MX8 updated to remove proxy settings');
    } else {
        print 'MX8 not changed' . "\n";
        $log->info($email.'|MX8 not changed');
    }
}

###########################################################################
# resetUxUser
# Should only be run for initial state or for addressbook only in preload mode.
###########################################################################
sub resetUxUser($$$$) {
    my $host                 = shift;
    my $port                 = shift;
    my $email                = shift;
    my $telnetMasterPassword = shift;
    my $result               = "";

    ( my $user, my $domain ) = split( /\@/, $email );

    #
    # Telnet to host / port.
    #
    my $remote = new Net::Telnet( Timeout => 5, Errmode => 'return' );

    $remote->port($port);
    $remote->open($host);

    #
    # Issue commands to erase and clear cache.
    #

    $remote->waitfor('/>$/i');

    my $cmd = "login $telnetMasterPassword write";
    $remote->print($cmd);
    $remote->waitfor('/OK$/i')
      or print("\nExpected return for '$cmd' of 'OK' was not received.\n");

    $cmd = "cache clear user $email";
    $remote->print($cmd);
    $remote->waitfor('/OK$/i')
      or $result .= "Expected return for '$cmd' of 'OK' was not received.\n";

    if ( $port eq "5230" ) {
        $cmd = "cache clear calendar $email";
        $remote->print($cmd);
        $remote->waitfor('/OK$/i')
          or $result .= "Expected return for '$cmd' of 'OK' was not received.\n";
    }

    $cmd = "user $domain delete $user erase";
    $remote->print($cmd);
    $remote->waitfor('/OK$/i')
      or $result .= "Expected return for '$cmd' of 'OK' was not received.\n";

    #
    # If successful, output the following...
    #
    if ( $result eq "" ) {
        print("$email successfully erased from $host:$port.\n");
    } else {
        print($result);
    }

}

###########################################################################
# resetAccount
###########################################################################
sub resetAccount {
    my $email     = shift;
    my $state     = shift;
    my $mode      = shift;
    my $setActive = shift;
    my $setProxy  = shift;
    my $mos       = shift;
    my $LOGSTRING = "$email| ";

    my $pab_telnetMasterPassword = $cfg->param('pab_telnetManagementPassword');
    my $cap_telnetMasterPassword = $cfg->param('cap_telnetManagementPassword');

    my $dn;
    my $junk;
    my $calHost;
    my $pabHost;
    my $calLoc;
    my $pabLoc;
    my $migstatus;

    $state = lc($state);
    $mode  = uc($mode);

    my ( $result, $ldap ) = connectLDAP( $email, $cfg );

    #
    # Get CAL and PAB data for user from LDAP
    #
    ( $dn, $junk, $migstatus, $junk, $junk, $pabHost, $calHost, $pabLoc, $calLoc ) = lookupEmail( $email, $result, $ldap );

    if ($setActive) {
        print "Setting $email to active \n";
        setMailboxStatus( $email, $cfg, "A", $result, $ldap );
        goto END;
    }

    if ($setProxy) {
        print "Setting $email to proxy \n";
        setMailboxStatus( $email, $cfg, "P", $result, $ldap );
        goto END;
    }

    if ($SDEBUG) {
        print("DEBUG: email = $email\n");
        if ( defined($pabHost) ) {
            print("DEBUG: pabHost   = $pabHost\n");
        } else {
            print("DEBUG: pabHost   =\n");
        }
        if ( defined($pabLoc) ) {
            print("DEBUG: pabLoc    = $pabLoc\n");
        } else {
            print("DEBUG: pabLoc    =\n");
        }
        if ( defined($calHost) ) {
            print("DEBUG: calHost   = $calHost\n");
        } else {
            print("DEBUG: calHost   =\n");
        }
        if ( defined($calLoc) ) {
            print("DEBUG: calLoc    = $calLoc\n");
        } else {
            print("DEBUG: calLoc    =\n");
        }
        print("DEBUG: state    = $state\n");
        print("DEBUG: mode    = $mode\n");
    } else {
        print("Running delete user for $email in $state state and $mode mode. \n");
    }
 $migstatus = com::owm::migBellCanada::setMigstatus($email,'',$cfg,$log,$migstatus,'z'); 

    if ( $state eq 'p' ) {

        #for preload reset only delete the calendar
        #	resetUxUser($pabHost,'4211',$email); Address book is now being deleted within migrateBatch for preload cases
    } elsif ( $state eq 'i' ) {
        my $port;

        #delete the mailbox
        unless ($skipMSS) {
            if ( defined($pabHost) && defined($calHost) ) {
                $mos->deleteMailbox($email);
                if ( !$mos->is_success ) {
                    $log->error( "$LOGSTRING: Failed occurred while deleting the mailbox : " . $mos->{ERROR} );
                    print $mos->{ERROR};
                } else {
                    $log->debug("$LOGSTRING: deleted mailbox, did not recreate mss");
                    print "Deleted mailbox, did not recreate mss \n";
                }
            } else {
                $log->warn("$LOGSTRING: could not attempt mailbox delete as user has no calendar or addressbook configured");
                print "Skipped mailbox delete as missing pab/cal entries \n";
            }
        }

        #for initial reset, delete both calendar and addressbook
        if ( defined($pabHost) ) {
            ( $port, my $error ) = com::owm::migBellCanada::getPortNumber( 'telnet', 'pab', $pabHost, $log, $cfg );
            unless ( $error == 1 ) {
                resetUxUser( $pabHost, $port, $email, $pab_telnetMasterPassword );
                $log->info("$email| cleared on port $port for $pabHost");
                print("$email| cleared addressbook on port $port\n");
            } else {
                $log->error("$LOGSTRING: Could not clear addressbook - $port");
            }
        }
        if ( defined($calHost) ) {
            ( $port, my $error ) = com::owm::migBellCanada::getPortNumber( 'telnet', 'cal', $calHost, $log, $cfg );
            unless ( $error == 1 ) {
                resetUxUser( $calHost, $port, $email, $cap_telnetMasterPassword );
                $log->info("$email| cleared on port $port for $calHost");
                print("$email| cleared on calendar port $port\n");
            } else {
                $log->error("$LOGSTRING: Could not clear addressbook - $port");
            }
        }
    } else {
        $log->info("$email| Skip clearing of calendar or addressbook");
    }

    deleteAccLdapAttributes( $email, $cfg, $state, $mode );

    clear_preload_state( $email, $cfg ) if ( $state eq 'i' );

    # should this be done first?
    if ($state eq 'i') {
        unsetMx8Proxy( $email, $cfg, $log );
    }
  END:
    $ldap->unbind;
}

###########################################################################
# MAIN
###########################################################################
my $email;
my $calHost;
my $pabHost;
my $calLoc;
my $pabLoc;
my $dn;
my $junk;
my $ldapHost = $cfg->param('mx_dirserv_host');
my $ldapPort = $cfg->param('mx_dirserv_port');
my $ldapPwd  = $cfg->param('mx_dirserv_pwd');
my $state    = 'p';
my $mode     = 'WL';
my $setActive;
my $setProxy;
my $BATCHFILE;

#
# Parse command line.
#
GetOptions(
    '-d',          \$SDEBUG,      # Override debug in config file.
    'email=s',     \$email,
    'state:s',     \$state,
    'mode:s',      \$mode,
    'active',      \$setActive,
    'proxy',       \$setProxy,
    'skipmss',     \$skipMSS,
    'batchfile:s', \$BATCHFILE
) || usage();

# Create mOS object.
my $mosHost = $cfg->param('mosHost');
my $mosPort = $cfg->param('mosPort');
my $mos     = com::owm::mosApi->new( $mosHost, $mosPort );

$log->info("SCRIPT| Skip MSS is enabled") if ( defined $skipMSS );
print("Skip MSS is enabled\n") if ( defined $skipMSS and $SDEBUG );

# batchfile takes precedence, if there is one the email parameter is ignored
if ( ( !defined $BATCHFILE ) || ( $BATCHFILE eq '' ) ) {
    if ( ( !defined $email ) || ( $email eq '' ) ) {
        usage();
    } else {
        $mos->user($email);
        resetAccount( $email, $state, $mode, $setActive, $setProxy, $mos );
    }
} else {
    if ( -e $BATCHFILE ) {
        open( my $fh, "<", $BATCHFILE )
          or $log->logdie("SCRIPT|Can't open file $BATCHFILE: $! ");

        foreach my $line (<$fh>) {

            #$email = substr($line, 0, index($line, ","));  # Use everything before the first comma as email
            ( $email, $mode, my $realm ) = split( /,/, $line );
            $mos->user($email);
            resetAccount( $email, $state, $mode, $setActive, $setProxy, $mos );
        }

        close $fh;
    } else {
        $log->error("SCRIPT|$BATCHFILE does not exist");
    }
}
print("\n************************************************ \n\n");
exit 0;
