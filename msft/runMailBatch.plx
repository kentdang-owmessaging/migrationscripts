#!/usr/bin/perl
###########################################################################
#
#  Copyright 2016 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Author:      Gary Palmer <gary.palmer@owmessaging.com>
#  Date:        1Q/2016
#  Version:     1.0.0 - March 9, 2016
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;

# A package that provides an easy way to have a config file
use Config::Simple;

use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;

use Cwd;
use File::Spec;
use Getopt::Long;
use Parallel::Fork::BossWorker; 
use POSIX qw(strftime);

my $configPath = 'conf/migrator.conf';
my $configLog = 'conf/log.conf';

my $numThreads = 10;

# Logging object.
Log::Log4perl->init($configLog);
my $log = get_logger('MIGRATE');

my ($batchFile, $batchCmd, $toolDir, $JOBID, $mode, $debug);
GetOptions(
    'batchFile=s', \$batchFile,
    'JOBID=s', \$JOBID,
    'mode=s', \$mode,
    'debug', \$debug,
);

unless(defined($batchFile))
{
    $log->logdie('SCRIPT|FATAL: batchFile argument is required');
}

unless(-f $batchFile)
{
    $log->logdie('SCRIPT|FATAL: batchFile is not a file');
}

unless(-r $batchFile)
{
    $log->logdie('SCRIPT|FATAL: batchFile is readable');
}

unless(defined($JOBID))
{
    $log->logdie('SCRIPT|FATAL: JOBID argument is required');
}

unless(defined($mode))
{
    $log->logdie('SCRIPT|FATAL: mode argument is required');
}

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
    or $log->logdie('SCRIPT|FATAL: '.$cfg->error().'. Please fix the issue and restart the migration program. Exiting.');

my $mailtraceArchiveDir = $cfg->param('mailtraceArchiveDir');
my $archiveDir;
my $parseCounters_tool_cmd = $cfg->param('parseCounters_tool_cmd');

my $threadTimeout = $cfg->param('thread_timeout');

my ($javaToolCmd, $javaToolDir);

if ((uc($mode) eq 'MX8') || (uc($mode) eq 'MYI'))
{
    $javaToolCmd = $cfg->param('owm_migration_tool_batch_cmd');
    $javaToolDir = $cfg->param('owm_migration_tool_dir_preload');
}
elsif (uc($mode) eq 'WL')
{
    $javaToolCmd = $cfg->param('migration_tool_batch_cmd');
    $javaToolDir = $cfg->param('migration_tool_dir_preload');
}
else
{
    $log->error('SCRIPT|'.$batchFile.' Unknown migration type "'.$mode.'"');
    return;
}

sub dirEmpty
{
    if (!-e $_[0])
    {
        $_[1] = 'Directory does not exist.';
        return;
    }
    if (!-d $_[0])
    {
        $_[1] = 'Path does not reference a directory.';
        return;
    }
    if (!opendir(DIR, $_[0]))
    {
        $_[1] = 'Could not open directory.';
        return;
    }
    while ($_ = readdir(DIR))
    {
        next if m/^\.\.?$/;
        return;
    }
    return 1;
}
sub parseSummary 
{
    my $LOGSTRING = 'SCRIPT|'.$mode.':JOB-'.$JOBID;
    unless(system('mv', './log/mail/tracelogs/pl-migration-summary', $archiveDir.'/'))
    {
        if ($? == -1)
        {
            $log->error($LOGSTRING.':Could not archive summary log, failed to execute mv');
            return;
        }
        elsif (($? >> 8) != 0)
        {
            my $error = ($? >> 8);
            $log->error($LOGSTRING.':Could not archive summary log, mv exited with error code '.$error);
            return;
        }
        elsif ($? & 127)
        {
            my $signal = $? & 127;
            $log->error($LOGSTRING.':Could not archive summary log, mv exited with signal '.$signal);
            return;
        }
    }
    
    my $parseTimers_tool_cmd = $cfg->param('parseCounters_tool_cmd');
    if (defined $parseTimers_tool_cmd)
    {
        $log->info($LOGSTRING.': archived to "'.$archiveDir.'", parsing timers');

        $parseTimers_tool_cmd =~ s/<file>/$archiveDir/;
        $parseTimers_tool_cmd =~ s/<jobid>/$JOBID/;
        $parseTimers_tool_cmd =~ s/<email>/email/;
        my $doDebug = '';
        if ($debug)
        {
            $doDebug = 'd';
        }
        $parseTimers_tool_cmd =~ s/<debug>/timers $doDebug/;
        my $result = `$parseTimers_tool_cmd`;
        $log->info($LOGSTRING.': Invoked script to parse mail migration timers. "'.$parseTimers_tool_cmd.'" result: '.$result);
    }else
        {
            $log->info($LOGSTRING.': did not process timers. Archived to "'.$archiveDir.'"');
        }  
    return;        
}
sub worker
{
    my $work = shift;
    my $email = $work->{ email };
    my $error;
    my $LOGSTRING = $email.'|'.$mode.':JOB-'.$JOBID;
    my $traceLogName = $email;
    $traceLogName =~ s/@/_/;
    if (!dirEmpty('./log/mail/tracelogs/'.$traceLogName.'/', $error))
    {
        if (defined($error))
        {
            $log->warn($LOGSTRING.':Could not archive trace logs '.$error);
        }
        else
        {
            unless(system('mv', './log/mail/tracelogs/'.$traceLogName, $archiveDir.'/'))
            {
                if ($? == -1)
                {
                    $log->error($LOGSTRING.':Could not archive trace logs, failed to execute mv');
                    return;
                }
                elsif (($? >> 8) != 0)
                {
                    my $error = ($? >> 8);
                    $log->error($LOGSTRING.':Could not archive trace logs, mv exited with error code '.$error);
                    return;
                }
                elsif ($? & 127)
                {
                    my $signal = $? & 127;
                    $log->error($LOGSTRING.':Could not archive trace logs, mv exited with signal '.$signal);
                    return;
                }
            }

            my $parseCounters_tool_cmd = $cfg->param('parseCounters_tool_cmd');
            if (defined $parseCounters_tool_cmd)
            {
                $log->info($LOGSTRING.': archived to "'.$archiveDir.'", parsing counters');

                my $filename = File::Spec->catfile($archiveDir, $traceLogName);
                $parseCounters_tool_cmd =~ s/<file>/$filename/;
                $parseCounters_tool_cmd =~ s/<jobid>/$JOBID/;
                $parseCounters_tool_cmd =~ s/<email>/$email/;
                my $doDebug = '';
                if ($debug)
                {
                    $doDebug = 'd';
                }
                
                $parseCounters_tool_cmd =~ s/<debug>/$doDebug/;
                my $result = `$parseCounters_tool_cmd`;
                $log->info($LOGSTRING.': Invoked script to parse mail migration counters. "'.$parseCounters_tool_cmd.'" result: '.$result);
            }
            else
            {
                $log->info($LOGSTRING.': archived to "'.$archiveDir.'"');
            }
        }
    }
    else
    {
        $log->warn($LOGSTRING.':No mail trace logs to archive for this batch');
    }

    return;
}

mkdir $mailtraceArchiveDir unless -d $mailtraceArchiveDir;
my $timestamp = strftime('%Y%m%d%H%M%S', localtime());
$archiveDir = $mailtraceArchiveDir.'/JOB-'.$JOBID.'.'.$timestamp.'/';
my $LOGSTRING = 'SCRIPT|'.$batchFile.':JOB-'.$JOBID;
mkdir $archiveDir or $log->warn($LOGSTRING.': Could not make tracelog archive dir ('.$archiveDir.') '.$!);

my $batchFD;
my @users;
open($batchFD, '<', $batchFile) || die;
while(my $user = <$batchFD>)
{
    chomp($user);
    push(@users, $user);
}
close($batchFD);

my $pwdOrig = cwd();
$log->info("SCRIPT|$batchFile Calling Java Migration Tool ... from $javaToolDir ");
$log->info("SCRIPT|$batchFile cd to ".$javaToolDir) if $debug;
$javaToolCmd =~ s/<batchfile>/$batchFile/;
$log->debug("SCRIPT|$batchFile javaToolCmd = ".$javaToolCmd) if $debug;
chdir($javaToolDir);
$log->debug("SCRIPT|$batchFile Invoking mail migration batch with   ".$javaToolCmd."\n") if $debug;
my $return = `$javaToolCmd`;
$log->debug("SCRIPT|$batchFile javaToolCmd res = ".$return) if $debug;
chdir($pwdOrig);

# parse the trace logs in children as a large batch with users with large
# mailboxes could take a considerable time otherwise. 
# Use bossworker to limit the number of parseCounters processes running
# at any one time to stop consuiming all the resources

parseSummary();

my $bw = Parallel::Fork::BossWorker->new(
    work_handler   => \&worker,       # Routine that does the work.
    global_timeout => $threadTimeout, # In seconds.
    worker_count   => $numThreads,    # Number of worker threads.
);

foreach my $email (@users)
{
    #The batch file may have blank lines, do not process these to the worker.
    $bw->add_work({ email => $email }) if ( index($email, "@") != -1  );
}

$bw->process();
