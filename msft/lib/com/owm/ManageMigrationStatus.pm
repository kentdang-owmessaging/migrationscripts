#!/usr/bin/perl
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        Sep/2015
#  Version:     1.0.0 - Sep 23 2015
#
###########################################################################

package com::owm::ManageMigrationStatus;
use SOAP::Lite;

use MIME::Base64;
use strict;
use Date::Format;
use UUID::Generator::PurePerl;
use UUID::Object;
use DateTime;
use DateTime::Format::XSD;
use Sys::Hostname;

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

sub fault_($$) {

	my ($email, $log) = @_;
	$log->error($email . " soap call failed.");
	print "\non fault\n";
	my $som = undef;
	return $som;
}


sub ManageMigrationStatus($$$$$$$$$){
	my ($proxy, $wsdl, $email, $eventId ,$jobId, $jobDesc, $mailboxsequence, $mailboxCount, $log) = @_;
    my $server = hostname();
    my @h = split (/\./,$server);
    $server = $h[0];

	my $uri = 'http://telstra.com/sdfcore/bsc/mailboxMigration/mailboxStatusManagement/v1';
	my $soap = SOAP::Lite ->readable(1)  ->uri($uri)  ->service($wsdl) ->proxy($proxy);

	my $ug = UUID::Generator::PurePerl->new();
	my $uuid = $ug->generate_v3(uuid_ns_dns, 'www.telstra.com')->as_string();
    my $timeStamp = DateTime::Format::XSD->format_datetime( DateTime->now);
	
	my $requestApplicationLabel =	SOAP::Header->name('requestApplicationLabel' => 'OWMP') -> prefix("v1");
	my $requestSessionID =	SOAP::Header->name('requestSessionID' => $uuid)-> prefix("v1");
	my $requestTransactionID = SOAP::Header->name('requestTransactionID' => $uuid)-> prefix("v1");
	my $requestTimestamp =	SOAP::Header->name('requestTimestamp' => $timeStamp)-> prefix("v1");
	my $requestorId =	SOAP::Header->name('requestorId' => 'customer')-> prefix("v1");
	my $SDFConsumerRequestHeader = SOAP::Header->name('SDFConsumerRequestHeader')-> value (\SOAP::Header->value($requestApplicationLabel,$requestSessionID,$requestTransactionID,$requestTimestamp,$requestorId));
	$SDFConsumerRequestHeader ->  prefix("v1");
	$soap->headerattr({'xmlns:v1' => 'http://telstra.com/sdfcore/SDFConsumerRequestHeader/v1_0'});
	$soap->bodyattr({ 'xmlns:v11' => 'http://telstra.com/sdfcore/bsc/mailboxMigration/mailboxStatusManagement/v1' });
	
	my $status = SOAP::Data->name('Mailbox' => \SOAP::Data->value(
        SOAP::Data->name('MailboxEmailAddress' => $email) -> prefix("v11"),
		SOAP::Data->name('EventLog' => \SOAP::Data->value(
			 SOAP::Data->name('Event' => \SOAP::Data->value(
				SOAP::Data->name('EventId' => $eventId)-> prefix("v11"))-> prefix("v11"))-> prefix("v11"))-> prefix("v11"),
		SOAP::Data->name('Job' => \SOAP::Data->value(
			SOAP::Data->name('JobId' => $jobId)-> prefix("v11"),
			SOAP::Data->name('JobDescription' => $jobDesc)-> prefix("v11")-> prefix("v11"),
            SOAP::Data->name('Server' => $server) -> prefix("v11"),
            SOAP::Data->name('MailboxSequence' => $mailboxsequence) -> prefix("v11"),
            SOAP::Data->name('MailboxCount' => $mailboxCount) -> prefix("v11"))
		)-> prefix("v11"))-> prefix("v11"))-> prefix("v11"))->prefix("v11");

	
	$soap->on_fault(sub { fault_($email,$log) });
	 
	my $som = $soap->call('v11:UpdateMailboxStatusRequest', $status, $SDFConsumerRequestHeader);

	if (! defined $som) {
	 return ('',1);
	}
	
	if ($som->fault) {
		$log->warn($requestTransactionID.",".$requestSessionID.",".$requestApplicationLabel.",".$requestTimestamp.",".$email.$som->faultcode .",". $som->faultstring);
		return ('',1);
	 } else {
	 	my $requestApplicationLabel = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestApplicationLabel');
		my $requestTransactionID = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestTransactionID');
		my $requestSessionID = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestSessionID');
		my $requestTimestamp = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestTimestamp');

		my $status = $som->valueof('//UpdateMailboxStatusResponse/Status');
		my $JobId =  $som->valueof('//UpdateMailboxStatusResponse/Job/JobId');
		my $jobDesc =  $som->valueof('//UpdateMailboxStatusResponse/Job/JobDescription');
		
		if ( $status ne "OK"){
			$log->warn("$requestTransactionID, $requestSessionID, $requestApplicationLabel, $requestTimestamp, $email, $JobId, $mailboxsequence/$mailboxCount, $status");
			return ($status,1);
		} else {
			$log->info("$requestTransactionID, $requestSessionID, $requestApplicationLabel, $requestTimestamp, $email, $JobId, $mailboxsequence/$mailboxCount, $status");
			return ($status,0);
		}
	 }

}

1;
