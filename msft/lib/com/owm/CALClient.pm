#!/usr/bin/perl
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        July/2015
#  Version:     1.0.0 - July 11 2015
#
###########################################################################
package com::owm::CALClient;

use strict;
use LWP;
use XML::LibXML;

sub new {
	my $class  = shift;
	my %parm = @_;
	my $this = {};
	bless $this, $class;
}


sub doRequest($$$$$$$$){

	my ($url, $user, $password, $method, $contentType, $body, $headers, $DEBUG) = @_;
	my $ua = LWP::UserAgent->new;
	my $req = HTTP::Request->new($method, $url);
	$req->authorization_basic($user, $password);
	$req->header("Content-Type" => "$contentType" );
	if (defined($headers))
	{
		my %headers = %{$headers};
		foreach my $header (keys(%headers))
		{
			$req->header($header, $headers{$header});
		}
	}
	use Encode;
	$body = encode('UTF-8', $body);
	$req->content($body);
	print ("\n----------" . $req->as_string . "---------\n") if ($DEBUG); #debug
	my $resp = $ua->request($req);
	print ("\n----------" . $resp->as_string . "---------\n") if ($DEBUG); #debug
	return $resp;
}

###########################################
# propfind
# propfind method
# Return:
#	the http response	
############################################
sub propfind ($$$$){

	my ($url, $user, $password,$DEBUG) = @_;
	
	my $method = "PROPFIND";
	my $contentType = "text/xml; charset=utf-8";
	my $body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
	<x0:propfind xmlns:x0=\"DAV:\" xmlns:x1=\"http://calendarserver.org/ns/\" xmlns:x2=\"urn:ietf:params:xml:ns:caldav\" xmlns:x3=\"http://apple.com/ns/ical/\" xmlns:CP=\"http://www.cp.net/caldav\" xmlns:ns0=\"http://laszlosystems.com/webtop/DAV\">
	  <x0:prop>
		<CP:primary-calendar/>
		<ns0:color/>
		<ns0:name/>
		<x0:displayname/>
	  </x0:prop>
	</x0:propfind>";

	my $ua = LWP::UserAgent->new;
	my $req = HTTP::Request->new($method, $url);
	$req->authorization_basic($user, $password);
	$req->header("Content-Type" => "$contentType" );
	$req->header("Depth" => "1" );
	$req->content($body);
	
	my $resp = $ua->request($req);
  
	return $resp;

}
###########################################
# findPrimaryCal
# find user's primary calendar
# Return:
#	the url of primary calendar	
############################################
sub findPrimaryCal ($$$$) {
	my ($baseURL, $user, $password,$DEBUG) = @_;
	my $resp = propfind ($baseURL, $user, $password,$DEBUG);
	
	print "url=".$baseURL."\n" if ($DEBUG);

	if ($resp->code == 207) {
		my $xmlparser = XML::LibXML->new();
		my $doc = $xmlparser->parse_string($resp->content);#$resp->content->as_string);
		my @nodes = $doc->findnodes(XML::LibXML::XPathExpression->new("/D:multistatus/D:response/D:propstat[./D:status/text() = \"HTTP/1.1 200 OK\"]/D:prop"));

		foreach my $nn (@nodes){
			my $href = $nn->findnodes(XML::LibXML::XPathExpression->new("../../D:href"));
			my $primary = $nn->findnodes(XML::LibXML::XPathExpression->new("./ns2:primary-calendar"));
			my $color = $nn->findnodes(XML::LibXML::XPathExpression->new("./ns3:color"));;
			my $name = $nn->findnodes(XML::LibXML::XPathExpression->new("./ns3:name"));;
			my $displayname = $nn->findnodes(XML::LibXML::XPathExpression->new("./D:displayname"));;
			#print "$primary\n$color\n$name\n$displayname\n" if ($DEBUG) ;
			if ( $primary eq "true" ) {
					return $href;
			}
		}
	}else{
		return (undef);
	}
}

###########################################
# mkcalendar
# mkcalendar method
# Return:
#	the url of calendar	entry
############################################
sub mkcalendar ($$$$$$) {

	my ($url, $calDisplayName, $calName, $user, $password,$DEBUG) = @_;
	my $method = "MKCALENDAR";
	my $color = 7;
	my $contentType = "text/xml; charset=utf-8";
	my $body = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
	<C:mkcalendar xmlns:D=\"DAV:\"
			  xmlns:CP=\"http://www.cp.net/caldav\"
			  xmlns:ZZ=\"http://laszlosystems.com/webtop/DAV\"
			  xmlns:C=\"urn:ietf:params:xml:ns:caldav\">
		<D:set>
		<D:prop>
		  <D:displayname>$calDisplayName</D:displayname>
		  <ZZ:name>$calName</ZZ:name>
		  <ZZ:color>$color</ZZ:color>
		</D:prop>
		</D:set>
	</C:mkcalendar>";
	
	
	my $resp = doRequest($url, $user, $password, $method, $contentType, $body, undef, $DEBUG);
	
	if ($resp->code == 201) {
		return $url;
	}else{
		return undef;
	}
}

sub report{

	my ($url, $user, $password,$DEBUG) = @_;
	my $method = "";
	my $contentType = "text/xml; charset=utf-8";
	my $body = "";
	
	doRequest($url, $user, $password, $method, $contentType, $body, undef, $DEBUG);

}

###########################################
# post
# post the ical to calendar server
# Return:
#	the response of http request
############################################
sub post($$$$$){

	my ($url, $user, $password, $icalfile, $DEBUG) = @_;

	my $method = "POST";
	my $contentType = "text/calendar; charset=utf-8";
	
	open(FILE, $icalfile) || die "can not open the file: $!";
	my $old_slash = $/; # remember the old value for line separator
	$/ = undef; # read the entire file at once
	my $body = <FILE>;
	$/ = $old_slash; # restore line separator

	my $resp = doRequest($url, $user, $password, $method, $contentType, $body, undef, $DEBUG);

	if ($resp->code == 200) {
	print "200\n" if ($DEBUG); 
	
	} else {
	print "not 200\n" if ($DEBUG);
	}

	return $resp;
}

###########################################
# postString
# post the ical to calendar server
# Return:
#	the response of http request
############################################
sub postString($$$$$){

	my ($url, $user, $password, $body, $DEBUG) = @_;

	my $method = "POST";
	my $contentType = "text/calendar; charset=utf-8";
	my $resp = doRequest($url, $user, $password, $method, $contentType, $body, undef, $DEBUG);
	if ($resp->code == 200) {
	print "200\n" if ($DEBUG); 
	
	} else {
	print "not 200\n" if ($DEBUG);
	}

	return $resp;
}

###########################################
# putString
# HTTP PUT the ical to calendar server.
# Return:
#	the response of http request
############################################
sub putString($$$$$) {
	my ($url, $user, $password, $body, $DEBUG) = @_;

	my $method = 'PUT';
	my $contentType = "text/calendar; charset=utf-8";
	my %headers = ( 'If-None-Match' => '*' );
	
	my $resp = doRequest($url, $user, $password, $method, $contentType, $body, \%headers, $DEBUG);

	if ($resp->code == 200) {
		print "200\n" if ($DEBUG); 
	} else {
		print "not 200\n" if ($DEBUG);
	}

	return $resp;
}

#nouse
sub importCal{
	my ($baseURL, $calDisplayName, $calName, $user, $password, $icalfile, $DEBUG ) = @_;
	my $resp = mkcalendar($baseURL, $calDisplayName, $calName, $user, $password, $DEBUG);

	if ($resp->code == 200) {
		my $url = $baseURL.$calName ;
		$resp = post($url,$user, $password,$icalfile,$DEBUG);
		if ($resp->code == 200) {
			#log 
		} else {
			#log
		}
		
	} else {
		print "";
	}
	

}

1;
