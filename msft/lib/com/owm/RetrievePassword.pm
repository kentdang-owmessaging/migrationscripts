#!/usr/bin/perl
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        Sep/2015
#  Version:     1.0.0 - Sep 23 2015
#
###########################################################################
package com::owm::RetrievePassword;
#use SOAP::Lite +trace;
use SOAP::Lite;
use MIME::Base64;
use Crypt::Mode::ECB;
use Crypt::CBC;
use Crypt::Cipher::AES;
use strict;
use Date::Format;
use UUID::Generator::PurePerl;
use UUID::Object;
use DateTime;
use DateTime::Format::XSD;

use Crypt::DES_PP;
use Getopt::Long;

sub fault_($$) {

	my ($userNameValue, $log) = @_;
	$log->error($userNameValue . " failed");
	my $som = undef;
	return $som;
}


sub retrievePassword {
	my ($proxy, $userNameValue, $emailValue, $sourceNameValue, $log, $keyFile) = @_;
    #keyFile is optional, if provided the method assumes we are using the V3 password retrieval method

	my $uri = 'http://telstra.com/sdfcore/bsc/BPSCredentialManagement/messages/v1';
	my $soap = SOAP::Lite ->readable(1)  ->uri($uri)  ->proxy($proxy); #, ssl_opts => [ SSL_verify_mode => 0 ]
	
	my $ug = UUID::Generator::PurePerl->new();
	my $uuid = $ug->generate_v3(uuid_ns_dns, 'www.telstra.com')->as_string();
	#my $timeStamp = time2str("%Z,%Y-%m-%dT%T\n", time());
    my $timeStamp = DateTime::Format::XSD->format_datetime( DateTime->now);
	
	my $requestApplicationLabel =	SOAP::Header->name('requestApplicationLabel' => 'TTest') -> prefix("v1");
	my $requestSessionID =	SOAP::Header->name('requestSessionID' => $uuid)-> prefix("v1");
	my $requestTransactionID = SOAP::Header->name('requestTransactionID' => $uuid)-> prefix("v1");
	my $requestTimestamp =	SOAP::Header->name('requestTimestamp' => $timeStamp)-> prefix("v1");
	my $requestorId =	SOAP::Header->name('requestorId' => 'customer')-> prefix("v1");
	my $SDFConsumerRequestHeader = SOAP::Header->name('SDFConsumerRequestHeader')-> value (\SOAP::Header->value($requestApplicationLabel,$requestSessionID,$requestTransactionID,$requestTimestamp,$requestorId));
	$SDFConsumerRequestHeader ->  prefix("v1");
	$soap->headerattr({'xmlns:v1' => 'http://telstra.com/sdfcore/SDFConsumerRequestHeader/v1_0'});
    

	
	my $userName = SOAP::Data->name('UserName' => $userNameValue);
	my $emailAddress = SOAP::Data->name('EmailAddress' => $emailValue);
	my $sourceName = SOAP::Data->name('SourceName' => $sourceNameValue);
	
	$soap->on_fault(sub { fault_($userNameValue,$log) });
	 
	my $som = $soap->RetrievePasswordRequest($userName, $emailAddress, $sourceName, $SDFConsumerRequestHeader);

	if (! defined $som) {
#	 print "undef\n";
	 return ('',1);
	}
	
	if ($som->fault) {
		$log->warn($requestTransactionID.",".$requestSessionID.",".$requestApplicationLabel.",".$requestTimestamp.",".$userNameValue.$som->faultcode .",". $som->faultstring);
		return ('',1);
	 } else {
	 	my $requestApplicationLabel = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestApplicationLabel');
		my $requestTransactionID = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestTransactionID');
		my $requestSessionID = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestSessionID');
		my $requestTimestamp = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestTimestamp');
		my $passowrd = $som->valueof('//RetrievePasswordResponse/Password');
		my $returnCode = $som->valueof('//RetrievePasswordResponse/ReturnStatus/ReturnCode');
		my $returnMessage =  $som->valueof('//RetrievePasswordResponse/ReturnStatus/ReturnMessage');
		if ( $returnCode ne "0"){
			$log->warn($requestTransactionID.",".$requestSessionID.",".$requestApplicationLabel.",".$requestTimestamp.",".$userNameValue.", ".$returnMessage);
			return ('',1);
		} else {
			$log->info($requestTransactionID.",".$requestSessionID.",".$requestApplicationLabel.",".$requestTimestamp.",".$userNameValue.", ".$returnMessage);
			# decrypt
            if (defined $keyFile) {
                #password will be encrypted if keyFile is passed to this method
                open(FH, "<", $keyFile ) or die "cannot open < $keyFile: $!";
                binmode(FH); 
                read(FH, my $key, 32, 0);
                close(FH);
                
                $passowrd = decode_base64($passowrd);
                my $iv = substr($passowrd,0,24);
                $iv =  decode_base64($iv );
                my $encryptedPassword = substr($passowrd,24);
                $encryptedPassword =  decode_base64($encryptedPassword );
                my $cbc = Crypt::CBC->new(-literal_key => 1, -cipher=>'Cipher::AES', -key=>$key, -iv=>$iv ,  -header=> 'none');
                my $ciphertext = $cbc->decrypt($encryptedPassword);
                return ($ciphertext,0);
            } else {
                #return clear text password
                return($passowrd,0);
            }
		}
	 }
}

sub encryptDESPassword($) {
    my($password) = @_;
    my $cipher = Crypt::DES_PP->new("\x34\x87\x20\x02\x27\x12\xcc\xff");
    my $pos=0; my $encrypted="";
	while ($pos < length($password)) {
		$encrypted .= unpack("H16",$cipher->encrypt(pack("a8", substr($password, $pos)))  );
		$pos += 8;
	 }
	return uc $encrypted;
}

sub decryptDESPassword($) {
	my $pwd=shift;
    my $cipher = Crypt::DES_PP->new("\x34\x87\x20\x02\x27\x12\xcc\xff");
	my $dec=""; my $p;
	my @parts = $pwd =~ /([0-9a-f]{16})/gi;
	foreach $p (@parts) { $dec.=$cipher->decrypt(pack("H16",uc($p)));	}
	if ($dec =~ /^(.+?)\x00/) {	$dec=$1; 	}
	return $dec;
}

1;



