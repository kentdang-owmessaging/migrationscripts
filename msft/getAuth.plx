#!/usr/bin/perl
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        Mar/2016
#  Version:     1.0.0 - Mar 05 2016
#
###########################################################################
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";
use Config::Simple;
use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;  
use LWP::UserAgent;

use SOAP::Lite;
use com::owm::RetrievePassword;

my $configPath = "$FindBin::Bin/conf/migrator.conf";
my $logPath = "$FindBin::Bin/conf/log2.conf";
#log object
Log::Log4perl->init($logPath);
my $log = get_logger("AUTH");

# Config object.
my $cfg  = new Config::Simple();
$cfg->read($configPath);

my $password_endpoint = $cfg->param('password_endpoint');
my $password_retrieval_by_file = $cfg->param('password_retrieval_by_file');
my $password_endpoint_file = $cfg->param('password_endpoint_file');
my $keyFile = $cfg->param('key_file');

my $email = $ARGV[0];
my ($pwd,$pwdError);
##

unless (defined $password_endpoint || defined $password_endpoint_file) {
    $log->error("not define password_endpoint in migration.conf");
    exit(1);
}
if ($password_retrieval_by_file eq "true") {
    ($pwd,$pwdError) = com::owm::RetrievePassword::retrievePassword($password_endpoint_file,$email,$email,"OWMP",$log); #clear text stub version
    $log->debug("$email| Getting password via flat file");
} else {
    ($pwd,$pwdError) = com::owm::RetrievePassword::retrievePassword($password_endpoint, $email,$email,"OWMP", $log, $keyFile);
    $log->debug("$email| Getting password via api");
    if ($pwdError eq 1) {
        #could not retrieve the password via the Telstra interface try the flat file
        if (defined $password_endpoint_file) {
            ($pwd, $pwdError) = com::owm::RetrievePassword::retrievePassword($password_endpoint_file,$email,$email,"OWMP",$log); 
            $log->debug("$email| API Failed, trying password via flat file");
        }
    }
}
if ( $pwdError eq 0 ){
    print "$pwd";
#       $pwd=com::owm::RetrievePassword::encryptDESPassword($pwd);
#        print "{DES}$pwd";
    exit (0);
} else {
    exit (1);
}        
        
$log->info($requestTransactionID.",".$requestSessionID.",".$requestApplicationLabel.",".$requestTimestamp.",".$userNameValue."completed");