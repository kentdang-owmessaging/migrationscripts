#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Mandi Buswell <mandi.buswell@owmessaging.com>
#  Date:        July/2015
#  Version:     1.0.0 - July 11 2015
#
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use Getopt::Long;
use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;  
use com::owm::mosApi;
use com::owm::migBellCanada;
use Net::LDAP;
use Digest::MD5;

# A package that provides an easy way to have a config file
use Config::Simple;

# A package that provides an easy way to launch multiple threads, with returns.
use Parallel::Fork::BossWorker; 

# A package that provides some status for our program
use Term::ProgressBar;

# Needed for LDAP search.
use Net::LDAP;

###########################################################################
# Global variables
###########################################################################

$| = 1;    ## Makes STDOUT flush immediately
my $SDEBUG;
my $PROCESSID = $$;
my $DBMESSAGE = '';
my $DBSTATUS;
my $DBSUCCESS = 0;
my $SCRIPTNAME = "update4Mx9";
my $dbmigtype = '';

our $configPath = 'conf/migrator.conf';
our $logPath = 'conf/log.conf';

# Varables for progress bar
my $progress;
my $count      = 0;
my $startcount = 0;

#Update options
our $PROVISIONREALM;
our $UPDATEMX;
our $SILENT;

# Logging object.
Log::Log4perl->init($logPath);
our $log = get_logger("PREP.PUID");
our $conflictLog = get_logger("PREP.CNFL");
our $eventLog = get_logger("EVENT");

# Boss Working object.
my $bw;
# Boss Working object for second pass in UPDATEMX mode
my $bw_second;

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
  or $log->logdie("SCRIPT|FATAL: ".$cfg->error()."\nINFO: Please fix the issue and restart the migration program. Exiting.\n\n");

  my $cosId = $cfg->param('defaultCos');
  my $password = $cfg->param('defaultPwd');
  


my $JOBID = $com::owm::migBellCanada::DEFAULT_JOBID;
my $start = time;

######################################################
# USAGE
######################################################
sub usage {

	print STDERR qq(

  Utility to update attributes in SUR 

  Command line to run script:

    update4Mx9.plx -puidFile <puidFile>  [-updatemx] [-realm]
                                          [-d]

    where:
    <puidFile>     is the path to a file containing email address of accounts to be updated.

    Switches:
    -d              turn debug on.

) unless $SILENT;

if ($SILENT) {
	print $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
}

	exit(0);
}

############################################################
## - Progress Init -                                       #
##   Initialize our progress bar based on addrs read in    #
############################################################
sub progress_init($) {
  my $size = shift;
  $count = 0;
  if ($UPDATEMX)
  {
    # need to allow for both passes (before and after replication delay)
  	$progress = Term::ProgressBar->new( { count => $size * 2, term_width => '100', ETA => 'linear',} );
  }
  else
  {
  	$progress = Term::ProgressBar->new( { count => $size, term_width => '100', ETA => 'linear',} );
  }
  $progress->minor(0);
  $progress->max_update_rate(2);
}

sub worker_second
{
	my $work = shift;
	my $email = $work->{data_line};
	my $status = $com::owm::migBellCanada::STATUS_SUCCESS;
    my $returnStatus;

	my $mosHost = $cfg->param('mosHost');
	my $mosPort = $cfg->param('mosPort');
	my $mos = com::owm::mosApi->new($mosHost,$mosPort);
	if (!$mos->is_success)
	{
		$status = $com::owm::migBellCanada::STATUS_FAIL_MOS_CONNECT;
		$log->error("$email|$status:Failed to create mOS object");
		goto END;
	}
	# Set debug level for mOS object.
	$mos->debugOn if $SDEBUG; 

	# Set additional object classes by reading. mxos.properties autoAddObjectClassesEnabled=true  must be set
	$log->info("$email|Start processing update for Mx9 part 2 ...");
	$mos->getUserBase($email);
	if (!$mos->is_success)
	{
	  $status = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_UPDATE;
	  $log->error("$email|Failed to update the mailbox with objectclasses: ".$mos->{ERROR});
	  goto END;
	}
	else
	{
		$log->debug("$email|Successfully set mailbox objectclasses ...");
		my $msgStoreHost = $work->{msgStoreHost};
		my $autoReplyHost = $work->{autoReplyHost};
		# Realm, msgStoreHost and autoReplyHost cannot be set by mos, need to use LDAP		
		$returnStatus = com::owm::migBellCanada::setProxyHosts("mss",$email,$msgStoreHost,$autoReplyHost,"SYD",$cfg,$log,$SDEBUG);
		if ($returnStatus eq $com::owm::migBellCanada::STATUS_SUCCESS )
		{
			$log->debug("$email| Provisioned MSS hosts");
			if ( $cfg->param('setProxyBy') eq "user" )
			{
				my $imapProxyHost = $work->{imapProxyHost};
				my $popProxyHost = $work->{popProxyHost};
				my $smtpProxyHost = $work->{smtpProxyHost};
				$log->debug("$email|Updating proxy hosts ...");
				# Now issue a second create call to update the proxy details
				com::owm::migBellCanada::setProxyHosts("proxy",$email,$imapProxyHost,$popProxyHost,$smtpProxyHost,$cfg,$log,$SDEBUG);
			}
			else
			{
				$log->debug("$email|Set proxy is by " . $cfg->param('setProxyBy') . "...");
				com::owm::migBellCanada::setProxyHosts("proxyrm",$email,"none","none","none",$cfg,$log,$SDEBUG);
			}
		}
	}
	
	  END:
	  $log->info("$email|End of processing part 2...");
	  if ($status eq $com::owm::migBellCanada::STATUS_SUCCESS)
	  {
		  return(1)
	  }
	  else
	  {
		  my %empty = ( 'empty' => 1);
		  return(\%empty);
	  }
	  print ""; #forces the progress bar to update. Don't know why, but without it will not update till the end.

}
######################################################################
## - worker -                                                       ##
######################################################################
##MCB UDPATE
sub worker {
  my $work = shift;
  my $status;
  my $errorCode;
  my $mbStatus = 'proxy';

  # Declare variables needed for migBellCanada methods, but are not required by this script.
  my $mailboxstatus;
  my $migstatus;
  my $consentstatus;
  my $maillogin;

  # Input file is list of at least email addresses (one per line)
  
   ##################################################################
  # General setup needed
  ##################################################################


  my $email = $work->{data_line};
  
  # Create mOS object.
  my $mosHost = $cfg->param('mosHost');
  my $mosPort = $cfg->param('mosPort');
  my $mos = com::owm::mosApi->new($mosHost,$mosPort);
  if (!$mos->is_success) {
    $status = $com::owm::migBellCanada::STATUS_FAIL_MOS_CONNECT;
    $log->error("$email|$status:Failed to create mOS object");
    goto END;
  }
  # Set debug level for mOS object.
  $mos->debugOn if $SDEBUG; 

  if($UPDATEMX) {
     # Update the mailbox.
	#my $email = $work->{data_line};
	
	$log->info("$email|Start processing update for Mx9 part 1 ...");
	$mos->userUpdateMailbox('proxy-nowait', $email, $mbStatus, $cosId, 'none', $cfg, $log);
	if ($mos->is_success) {
		$status = $com::owm::migBellCanada::STATUS_SUCCESS;
		$log->debug("$email|Successfully set status and cos ...");
	} else {
		  $status = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_UPDATE;
		  $log->error("$email|Failed to update the mailbox: ".$mos->{ERROR});
	}
	
	goto END;
  }
  if($PROVISIONREALM) {

	my $cosId = $work->{cosId};
	my $realm = $work->{realm};
	my $calStoreHost = $work->{calStoreHost};
	my $pabStoreHost = $work->{pabStoreHost};
	
	$log->info("$email| Start processing provision realm ...");
	#Perform the mos read here incase updatemx has not been run
	$mos->getUserBase($email);

	if (!$mos->is_success) {
	  $status = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_UPDATE;
	  $log->error("$email|Failed to update the mailbox with objectclasses: ".$mos->{ERROR});
	  goto END;
	 } else {
		$log->debug("$email|Successfully set mailbox objectclasses ...");
	}
	#Realm, msgStoreHost and autoReplyHost cannot be set by mos, need to use LDAP	
	
	my $returnStatus = com::owm::migBellCanada::setProxyHosts("realm",$email,"none","none",$realm,$cfg,$log,$SDEBUG);
	if ( $returnStatus eq $com::owm::migBellCanada::STATUS_SUCCESS ) {
		$log->debug("$email| Provisioned realm hosts (setProxyHosts) by ldap");
		#provision the pabcal first 
		$mos->userProvisionRealm($email,$password,$cosId,$realm,"none","none",$calStoreHost,$pabStoreHost);
		if (!$mos->is_success) {
		  $status = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_UPDATE;
		  $log->error("$email| Failed to provision the realm hosts (userProvisionRealm) : ".$mos->{ERROR});
		  print("$email| Failed to provision the realm hosts (userProvisionRealm) : ".$mos->{ERROR} . "\n") if ($SDEBUG); 
		 goto END;
		} else {
			#update the realm 
			$log->debug("$email| Provisioned realm hosts by mos");
			#provision the MSS 
			$mos->userProvisionMSS($email,$password,$cosId);
			if (!$mos->is_success ) {
			  $status = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_CREATE;
			  $log->info("$email| Failed to provision the mailbox : ".$mos->{ERROR} . ".  Likely already exists. Continuing .... \n");
			  print("$email| Failed to provision the mailbox : ".$mos->{ERROR} . ". Likely already exists. Continuing .... \n") if ($SDEBUG);	
				#continue as mailbox failing due to already exists is okay
			} else {
				$log->debug("$email| Provisioned mss hosts");
				$status = $com::owm::migBellCanada::STATUS_SUCCESS;
			}		  
			
		}

	} else {
		  $log->error("$email| Failed to provision the realm hosts (setProxyHosts) : ".$returnStatus);
	}
  }
  
  END:
  if($UPDATEMX)
  {
	$log->info("$email|End of processing part 1...");
  }
  else
  {
	$log->info("$email| End of processing ...");
  }

  if ($UPDATEMX && ($status == $com::owm::migBellCanada::STATUS_SUCCESS))
  {
  	  return($work)
  }
  else
  {
      my %empty = ( 'empty' => 1);
  	  return(\%empty);
  }
	print ""; #forces the progress bar to update. Don't know why, but without it will not update till the end.

}


######################################################################
## - Cleanup -                                                      ##
##   This routine is used to update the progress meter              ##
######################################################################
sub cleanup
{
    $progress->update( ++$count ) unless $SILENT;  # Update the counter
    my $work = shift;
    if (defined($work) && defined($work->{data_line}))
    {
    	$bw_second->add_work($work);
    }
}

######################################################################
## - Cleanup -                                                      ##
##   This routine is used to update the progress meter              ##
######################################################################
sub cleanup_second
{
    $progress->update( ++$count );  # Update the counter
}

######################################################################
## - hashFunc -                                                     ##
##   This routine is used to calculate a hash of the passed string  ##
##   It is used by getHosts to allocate users to a PAB/CAL server   ##
######################################################################
sub hashFunc
{
    my $str = shift;

    my $ctx = Digest::MD5->new;
    $ctx->add($str);
    # limit to 32 bits of result
    my $hash = oct('0b'.unpack('B32', $ctx->digest));
    return($hash & 0x7fffffff);
}


######################################################################
## - getHosts -                                                     ##
##   This routine is used to get the default or hosts for proxy     ##
##    and realm provisioning										##
######################################################################
sub getHosts ($$$$$){
  my $mode = shift;
  my $realm = shift;
  my $cfg = shift;
  my $log = shift;
  my $email = shift;
  
  my $calStoreHost;
  my $pabStoreHost;
  if ( $mode eq "realm" ) {
	  $log->debug("SCRIPT|getting hosts for $mode and realm $realm");
#	  my $thisRealm = $cfg->param('defaultRealm');
	  my $thisRealm = $realm;
	  #determine the REALM hosts and round robin through the options
	  if ($thisRealm eq $cfg->param('melRealm') ) {
		  $calStoreHost = $cfg->param('calStoreHostMel');
		  $pabStoreHost = $cfg->param('pabStoreHostMel');
	  } else {
		 $calStoreHost = $cfg->param('calStoreHostSyd');
		 $pabStoreHost = $cfg->param('pabStoreHostSyd');
	 }

     # Check if a list of PAB and/or CAL hosts was provided in the
     # configuration file.  If a list was provided, then allocate the 
     # user to one host from the list
     # Note that it checks for PAB and CAL individually so that in theory
     # one can be a list and the other a single value, although that is
     # unlikely.
     if (($calStoreHost =~ /,/) || (ref $calStoreHost eq 'ARRAY') ||
         ($pabStoreHost =~ /,/) || (ref $pabStoreHost eq 'ARRAY'))
     {
        # precompute the hash for both PAB and CAL rather than doing the
        # computation twice.
        my $hash = hashFunc($email);
        # If the list was in double quotes in the config file, it is returned
        # as-is, so check for a string with a comma in it
        if ($calStoreHost =~ /,/)
        {
           my @hosts = split(/,/, $calStoreHost);
           # the shift is likely not necessary here, but it was used in another
           # migration as the lower order bits were used for something else.
           # Use the modulus to get the offset into the list of hosts
           my $offset = ($hash >> 11) % scalar(@hosts);
           $calStoreHost = $hosts[$offset];
        }
        # If the list was NOT quoted in the config file, then it is returned
        # as an array reference.  Check for that
        if (ref $calStoreHost eq 'ARRAY')
        {
           my @hosts = @{$calStoreHost};
           # the shift is likely not necessary here, but it was used in another
           # migration as the lower order bits were used for something else.
           # Use the modulus to get the offset into the list of hosts
           my $offset = ($hash >> 11) % scalar(@hosts);
           $calStoreHost = $hosts[$offset];
        }
        # If the list was in double quotes in the config file, it is returned
        # as-is, so check for a string with a comma in it
        if ($pabStoreHost =~ /,/)
        {
           my @hosts = split(/,/, $pabStoreHost);
           # the shift is likely not necessary here, but it was used in another
           # migration as the lower order bits were used for something else.
           # Use the modulus to get the offset into the list of hosts
           my $offset = ($hash >> 11) % scalar(@hosts);
           $pabStoreHost = $hosts[$offset];
        }
        # If the list was NOT quoted in the config file, then it is returned
        # as an array reference.  Check for that
        if (ref $pabStoreHost eq 'ARRAY')
        {
           my @hosts = @{$pabStoreHost};
           # the shift is likely not necessary here, but it was used in another
           # migration as the lower order bits were used for something else.
           # Use the modulus to get the offset into the list of hosts
           my $offset = ($hash >> 11) % scalar(@hosts);
           $pabStoreHost = $hosts[$offset];
        }
     }
	 $log->debug("SCRIPT|getting hosts by realm returns cal: $calStoreHost, pab: $pabStoreHost");
	 # Use config realm map to get correct owm realm for home location

	 $thisRealm = $cfg->get_param($realm);
	 return ($thisRealm, $calStoreHost, $pabStoreHost);
  }
  if ( $mode eq "proxy" ) {
    $log->debug("SCRIPT|getting hosts for $mode");
	my $imapProxyHost = $cfg->param('imapProxyHost');
	my $popProxyHost = $cfg->param('popProxyHost');
	my $smtpProxyHost = $cfg->param('smtpProxyHost');
	my $msgStoreHost = $cfg->param('defaultMsgStoreHost');
	my $autoReplyHost = $cfg->param('defaultAutoReplyHost');

	return ($imapProxyHost,$popProxyHost,$smtpProxyHost, $msgStoreHost, $autoReplyHost);
  }
  

}

###################################################################
## - BuildQueue -                                                 #
##   Get the email addrs, puid and populate the processing queue  #
###################################################################
sub buildQueue {
  my $in_file = shift;
  $log->debug("SCRIPT|Input file set to \'$in_file\'");

  print("Main processing progress...\n") unless $SILENT;

  open my $file, "<", $in_file
    or $log->logdie("SCRIPT|Unable to open file $in_file: $!");

  while ( my $data_line = <$file>) 
  {
    $startcount++;
    chomp($data_line);
	if ( $UPDATEMX ) {
		my ($imapProxyHost, $popProxyHost, $smtpProxyHost, $msgStoreHost, $autoReplyHost) = getHosts("proxy","none",$cfg,$log, undef); 
		my @workerArray = split(/\,/,$data_line);
		my $email = $workerArray[0];
		$bw->add_work( { data_line => $email, imapProxyHost => $imapProxyHost, popProxyHost => $popProxyHost, smtpProxyHost => $smtpProxyHost,
						msgStoreHost => $msgStoreHost, autoReplyHost => $autoReplyHost		} );
	} elsif ( $PROVISIONREALM ) {
	    my ($email,$type,$realm) = split(/,/,$data_line);
		if (! defined $realm ) {
			$log->error("$email|$file realm undefined cannot process migration. Skipping this user");
		}elsif (! defined $type) {
			$log->error("$email|$file migration type undefined cannot process migration. Skipping this user");
		}else {
			($realm, my $calStoreHost, my $pabStoreHost) = getHosts("realm",$realm,$cfg,$log, $email);
			$bw->add_work( { data_line => $email, password => $password, cosId => $cosId, realm => $realm, 
							calStoreHost => $calStoreHost, pabStoreHost => $pabStoreHost  } );
		}
	} else {
		#No option was passed, we do not have any default action
		$log->error("SCRIPT|No processing ");
		usage();
	}
  }
  
  close($file);

  $log->debug("SCRIPT|Processing $startcount records read from input file.");
  print("Processing $startcount records.\n") unless $SILENT;

  return ($startcount);
}

######################################################
# ---------------------------------------------------
# Start Main
# ---------------------------------------------------
######################################################

$SDEBUG = $cfg->param('log_debug');

my $numThreads = $cfg->param('number_of_threads');
my $threadTimeout = $cfg->param('thread_timeout');
my $puidFile = '';

GetOptions( '-d',\$SDEBUG,               # Override debug in config file.
            '-puidFile=s',\$puidFile,
            'realm',\$PROVISIONREALM,
            'updatemx',\$UPDATEMX,
			'silent',\$SILENT,
            ) || usage();

print("\n\n") unless $SILENT;

if ($SDEBUG) {
  print("Running in DEBUG mode\n") unless $SILENT;
  $log->level($DEBUG);               #Override debug in log file when -d or debug set in config file
}

if ($SILENT) {
	#turn off debug on screen
	$SDEBUG = 0;
}

# Check input file exists
if ( -e $puidFile ) {
  $log->info("SCRIPT|******BEGIN Processing PUID input file: $puidFile");
} else {
  $log->error("SCRIPT|******BEGIN PUID input file is missing: $puidFile");
  usage();
} 

# Setup a boss to send out the work
$bw = Parallel::Fork::BossWorker->new(
  work_handler   => \&worker,       # Routine that does the work.
  result_handler => \&cleanup,      # Routine called after work is finish (optional).
  global_timeout => $threadTimeout, # In seconds.
  worker_count   => $numThreads );  # Number of worker threads.

if ($UPDATEMX)
{
    $bw_second = Parallel::Fork::BossWorker->new(
      work_handler   => \&worker_second, # Routine that does the work.
      result_handler => \&cleanup_second,       # Routine called after work is finish (optional).
      global_timeout => $threadTimeout,  # In seconds.
      worker_count   => $numThreads );   # Number of worker threads.
}

# Build queue that boss will handout to worker threads.
my $rc = &buildQueue($puidFile);

# Initialize the progess counters.
&progress_init($rc) unless $SILENT;

# Set boss to start handing out work to threads.
my $proc_start = time;
$bw->process();
my $proc_end = time;

if ($UPDATEMX)
{
	my $wait = $cfg->get_param('allowForReplication');
    my $proc_delta = $proc_end - $proc_start;
    my $delta = $wait - $proc_delta;
    if ($delta > 0)
    {
    	$log->info('Sleeping for '.$delta.' seconds to allow replication');
    	print "\n".'Sleeping for '.$delta.' seconds to allow replication ... ';
    	sleep($delta);
    	print 'done'."\n";
    }
	$log->info('Starting second part of updatemx process');
	$bw_second->process();
}

my $time_taken = time - $start;
my $dbh = com::owm::migBellCanada::connectEventDB($eventLog,$cfg,$log);	
if ($dbh eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR) {
	$log->error("SCRIPT|Database error, could not log event ");
	$log->error("SCRIPT|Event Log: JobId: $JOBID puidFile: $puidFile Finished,Took ".$time_taken." seconds: ".$DBMESSAGE);
} else {
	$log->debug("SCRIPT|Connected to event log database");
	com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::EVENT_UPDATEMX_START,'UpdateMX','',$puidFile,'','Finished','Took '.$time_taken.' seconds: '.$DBMESSAGE);
	$log->info("SCRIPT|closing database connection.");
	com::owm::migBellCanada::disconnectDB($dbh,$cfg,$log);
}


# Exit script gracefully.
print "\n" unless $SILENT;    # This just makes things look clean
if ($SILENT) {
	#if we get all the way here we must have succeeded so tell migrationWatcher
	print $com::owm::migBellCanada::STATUS_LOUD_SUCCESS;
}

exit 0;
######################################################
# ---------------------------------------------------
# End Main
# ---------------------------------------------------
######################################################

######################################################
# ---------------------------------------------------
# BEGIN POD Documentation
# ---------------------------------------------------
######################################################

__END__