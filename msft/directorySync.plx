#! /usr/bin/perl
#
############################################################################
### 'use' modules
#############################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl";
use lib "$FindBin::Bin/migperl/lib/perl5";
use strict;
use Net::LDAP;
use Getopt::Long;
use Config::Simple;
use Log::Log4perl qw(get_logger :levels);

###########################################################################
### Global variables
#############################################################################

$| = 1;    ## Makes STDOUT flush immediately
my $SDEBUG;
my $SRCLCN;
my $CONT;

my $configPath = 'conf/migrator.conf';
my $logPath    = 'conf/log.conf';

# Logging object.
Log::Log4perl->init($logPath);
my $logger = get_logger("SYNC");

#Initialise the configuration file
my $cfg = new Config::Simple();
$cfg->read($configPath)
  or $logger->logdie(
    "\nFATAL: " . $cfg->error() . "\nINFO: Please fix the issue and restart the migration program. Exiting.\n\n" );

my $newproxyflag = 0;
my $mailsetblock = 0;

my $lcnFile = $cfg->param('lastChangeNumberFile');
my $sleepWait = defined $cfg->param('sleepWait') ? $cfg->param('sleepWait') : 5;

my $srcMasterHost = $cfg->param('mx_dirserv_host_src');
my $srcMasterPort = $cfg->param('mx_dirserv_port_src');
my $srcMasterPass = $cfg->param('mx_dirserv_pwd_src');

my $destMasterHost = $cfg->param('mx_dirserv_host');
my $destMasterPort = $cfg->param('mx_dirserv_port');
my $destMasterPass = $cfg->param('mx_dirserv_pwd');

my $chglogDataFile = $cfg->param("chglogDataFile");
my $runOnce        = $cfg->param("runOnce");
my $stopFile       = $cfg->param("stopFile");
my $syncDir        = $cfg->param("syncDir");
my $ldapsearch     = $cfg->param('owmLdapsearch');
my $ldapmodify     = $cfg->param('owmLdapmodify');

my $popproxy  = $cfg->param('popProxyHost');
my $imapproxy = $cfg->param('imapProxyHost');
my $smtprelay = $cfg->param('smtpProxyHost');
my $mstore1   = $cfg->param('defaultMsgStoreHost');
my $mstore2   = $cfg->param('defaultMsgStoreHost');

my $tdir;
my $newdir;
my $TIMESTAMP = `date +"%Y%m%d%H%M%S"`;
my $ret;
my $lastHour = "99";
my $dir      = `pwd`;
my $nc;
my $HOUR = `date +"%H"`;
chomp $HOUR;
my $First;
my $mychglogfiles;
my $mod;
my $igood;
my $i34Error;
my $iredo;
my $cnt;
my $chgid;
my $timestamp;
my $processCN;    #this key holds the last change number processed
my $sourceLCN;    #this key holds the last change number on the source platform. represents where we need to process to
my $lastChangeProcessed;
my @a;
my $entryid;
my $na;

###########################################################################
### sub newdir
### rollover and archive logs and curlogs
#############################################################################

sub newdir {
    $tdir   = $_[0];
    $newdir = "$tdir-archive/$tdir.$timestamp";
    if ( -e $tdir ) {
        if ( $HOUR ne $lastHour ) {
            $logger->info("SCRIPT|Archiving $tdir Current hour $HOUR Last hour $lastHour");
            $logger->debug("SCRIPT|newdir TDIR $tdir NEWDIR $newdir");

            #archive first execution or every hour when running continuous
            `mv $tdir $newdir`;
            `mkdir $tdir`;
            if ( $tdir eq "$syncDir" ) {
                $logger->debug("SCRIPT|newdir Archiving the sync directory and restoring out lastchange number file. ");
                `cp $newdir/lastchangenumber $tdir/` if -e $newdir . "/lastchangenumber";
            }
        } else {
            $logger->debug("SCRIPT|Archiving skipped");
        }
    } else {
        $logger->debug("SCRIPT|newdir creating directory $tdir");
        `mkdir $tdir`;
    }
    return;
}

######################################################
### USAGE
########################################################
sub usage {
    print STDERR qq(
	Utility to update attributes in SUR
	Command line to run script:
	directorySync.pl  [-n <lastchangenumber>] [-d] [-runonce <true|false]
	[-blockautoreply] [-setnewproxy] [-apply] [-process]
where:
	<lastchangenumber>     provide a last change number
Switches:
	-d              turn debug on (overrides any setting config).
	-runonce            true=run once only, false=run continuously like a daemon (overrides any setting config)
	To stop when in continuous mode, either kill the script or touch the file $stopFile
	-blockautoreply Blocks the mailautoreplyhost changes being synched
	-setnewproxy    Create new accounts on the target system in Proxy mode
	-apply 			Process only the applying LDIFS from previously prepare ldif files in sync/tmp
	-process		Skip getting all the change logs from the source, and start processing using the file currently in the sync directory

	);

    exit(0);
}
######################################################
### ---------------------------------------------------
### Start Main
### ---------------------------------------------------
########################################################

$SDEBUG = $cfg->param('log_debug');
$logger->debug("SCRIPT|Begin password sync ... ");
my $mailsetblock = $cfg->param('blockautoreply');
my $newproxyflag = $cfg->param('setnewproxy');
my $getDestDN    = $cfg->param('getDestDN');
my $ignoreLdap34 = $cfg->param('ignoreLdap34');
defined $cfg->param('sleepWait') ? $cfg->param('sleepWait') : 5;
my $switch_negative = defined $cfg->param('switch_negative') ? $cfg->param('switch_negative') : "false";
my $ignoreProbe     = defined $cfg->param('ignoreProbe')     ? $cfg->param('ignoreProbe')     : "false";
my $useDefaultCos   = defined $cfg->param('useDefaultCos')   ? $cfg->param('useDefaultCos')   : "false";
my $defaultCos      = $cfg->param('defaultCos');
my $defaultMsgStoreHost = $cfg->param('defaultMsgStoreHost');
my $probeDN             = $cfg->param('probeDN');
my $startingPoint;
my $skip;
my $applyldif;
my $processldif;

#when script is starting remove any previous stopfile
`rm -rf $stopFile` if -e $stopFile;

GetOptions(
    '-d',              \$SDEBUG,         # Override debug in config file.
    '-n=s',            \$SRCLCN,         # Provide last change number
    '-runonce=s',      \$CONT,
    '-blockautoreply', \$mailsetblock,
    '-setnewproxy',    \$newproxyflag,
    '-apply',          \$applyldif,
    '-process',        \$processldif
) || usage();

print("\n\n");

if ( $applyldif || $processldif ) {
    $skip = "true";    #indicates we are not processing from the very beginning but starting at an intermediary step
}

if ($SDEBUG) {
    print("Running in DEBUG mode\n");
    $logger->info("SCRIPT|Running in DEBUG mode\n");
    $logger->level($DEBUG);    #Override debug in log file when -d or debug set in config file
}

#make all the directories needed if they do not exists
mkdir $syncDir        unless -d $syncDir;
mkdir "$syncDir/temp" unless -d "$syncDir/temp";
mkdir "$syncDir/redo" unless -d "$syncDir/redo";
mkdir "$syncDir/good" unless -d "$syncDir/good";

#mkdir "$syncDir/redo-archive" unless -d "$syncDir/redo-archive";
#mkdir "$syncDir/good-archive" unless -d "$syncDir/good-archive";

if ($CONT) {

    #override any configuration option
    $runOnce = $CONT;
}
unless ( $skip eq "true" ) {
    if ( -e $lcnFile ) {
        $lastChangeProcessed = `cat $lcnFile`;
        chomp($SRCLCN);
    }
    $logger->debug("SCRIPT| sync directory is $syncDir  starting change number is $SRCLCN lastchange run $lastChangeProcessed");
    if ( !$SRCLCN ) {
        if ( -e $lcnFile ) {
            $processCN = $lastChangeProcessed++;
            $logger->debug("SCRIPT|Last change number $processCN from file");
        } elsif ( $cfg->param('useDestLCN') eq "true" ) {

            # obtain last change number from Destination Master ..and start with it
            $processCN =
`$ldapsearch -D cn=root -w $destMasterPass -h $destMasterHost -p $destMasterPort -s base "(objectclass=dseroot)" lastchangenumber`;
            chomp $processCN;
            $processCN =~ s/\n//g;
            @a = split( "=", $processCN );
            $processCN = $a[1]++;
            $logger->debug("SCRIPT|Last change number $processCN from destination");

        } else {
            $logger->error(
                "SCRIPT|Last change number could not be found. Aborting run. Set manually to $lcnFile or pass as input");
            usage();
        }
    } else {
        $processCN = $SRCLCN;
        $logger->debug("SCRIPT|Last change number $processCN from command line");
    }
    $logger->info("SCRIPT|Source LastChangeNumber = $processCN\n");
    chomp $processCN;
    if ( ( $processCN lt 1 ) ) {
        $processCN = 1;
    }

    $logger->info("SCRIPT|Processing from last change number $processCN ... ");

    #Create LDAP Object to Source
    $logger->debug("SCRIPT|Attempting to connect to $srcMasterHost:$srcMasterPort");
    my $ldapSource = Net::LDAP->new( $srcMasterHost, port => $srcMasterPort );
    if ( !$ldapSource ) {
        $logger->error("SCRIPT|Connect to $srcMasterHost:$srcMasterPort failed");
        print "Error connecting to $srcMasterHost:$srcMasterPort \n" if ($SDEBUG);
        exit 0;
    }

    my $mesgSource = $ldapSource->bind( "cn=root", password => $srcMasterPass );
    if ( $mesgSource->code ne '0' ) {
        $logger->error( "SCRIPT|Bind to $srcMasterHost:$srcMasterPort failed. " . $mesgSource->code . " Check LDAP password." );
        exit 0;
    }
    $ldapSource->unbind;

    #Create LDAP Object to Destination

    my $ldapDest = Net::LDAP->new( $destMasterHost, port => $destMasterPort );
    if ( !$ldapDest ) {
        $logger->error("SCRIPT|Connect to $destMasterHost:$destMasterPort failed");
        exit 0;
    }

    my $mesgDest = $ldapDest->bind( "cn=root", password => $destMasterPass );
    if ( $mesgDest->code ne '0' ) {
        $logger->error( "SCRIPT|Bind to $destMasterHost:$destMasterPort failed. " . $mesgDest->code . " Check LDAP password." );
        exit 0;
    }
    $ldapDest->unbind;

} else {
    if ($applyldif)   { $startingPoint = "Apply Ldifs"; }
    if ($processldif) { $startingPoint = "Process Changelog"; }
    $logger->info("SCRIPT|Processing from  $startingPoint ... ");
    $sourceLCN = 10;
    $processCN = 9;
}

if ( $newproxyflag != 0 ) {
    $newproxyflag = 1;
}

# Check if we are going to block mailautoreplyhost from propogating
# #$mailsetblock = 0;

if ( $mailsetblock != 0 ) {
    $mailsetblock = 1;
}

my $ix = 1;
my $changetype;
my $mbx;
my $icc;
my $idel;
my $jx;

my @a;
my @b;
my $chgnumber;
my $mydn;
my $base;
my $i;
my $curlog;
my $line;
my $act;
my $mod;
my $igood;
my $iredo;
my $cnt;
my $oldstatus;

my $host9;
my $newstatus;
my $distmaster;
my $mailboxstatus;
my $status;
my $host;
my $n;

my $redoDir = "$syncDir/redo";
my $goodDir = "$syncDir/good";
my $tempDir = "$syncDir/temp";

my %entryidCache;

chomp $dir;
while ( $ix > 0 ) {
    $logger->info("SCRIPT|Directory synchronisation checking for changes ...");
    $logger->info("SCRIPT|Run once enabled ...") if ( $runOnce eq "true" );
    if ( -e $stopFile ) {

        # Break the program
        $logger->info("SCRIPT|Directory synchronisation stop file found, exiting ...");
        print("Directory sync script now stopping!\n") if ($SDEBUG);
        exit 0;
    }

    $HOUR = `date +"%H"`;
    chomp $HOUR;
    $timestamp = `date +"%Y%m%d%H%M%S"`;
    chomp $timestamp;

#archive the sync files from the last run and create a new blank sync directory, do not archive if skipping initial extract process
    newdir($syncDir) unless ( $skip eq "true" );
    $logger->debug("SCRIPT|Making all the directories redo $redoDir good $goodDir temp $tempDir sync $syncDir dir $dir ");
    mkdir $syncDir . "-archive" unless -d $syncDir . "-archive";
    mkdir $tempDir              unless -d $tempDir;
    mkdir $redoDir              unless -d $redoDir;
    mkdir $goodDir              unless -d $goodDir;

    #mkdir $syncDir."/redo-archive" unless -d $syncDir."/redo-archive";
    #mkdir $syncDir."/good-archive" unless -d $syncDir."/good-archive";
    #newdir($goodDir);
    if ( $HOUR ne $lastHour ) {
        $lastHour = $HOUR;
    }

    unless ( $skip eq "true" ) {

        # obtain last change number from the source system
        $sourceLCN =
`$ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort -s base "(objectclass=dseroot)" lastchangenumber`;
        chomp $sourceLCN;
        $logger->debug(
"SCRIPT|ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort -s base '(objectclass=dseroot)' lastchangenumber \n last change number from source $sourceLCN"
        );
        $sourceLCN =~ s/\n//g;
        @a = split( "=", $sourceLCN );
        $sourceLCN = $a[1];
    }

    #If the current change number on the source, is greater than the last time this script ran then there are changes to process
    if ( int($sourceLCN) > int($processCN) ) {
        $logger->info("SCRIPT|Run once enabled - changes to process");

#If the sourceLCN has been run remove the file and refresh - THIS PART IS WRONG because it assumes that the sourceLCN is the one that was last run resulting in the changelogdatafile which is not necessarily true
        unless ( $skip eq "true" ) {
            if ( -e "$syncDir/$chglogDataFile.$lastChangeProcessed" ) {
                `rm -rf $syncDir/$chglogDataFile.$lastChangeProcessed`;
            }
            if ( -e "$syncDir/$chglogDataFile" ) {
                `mv $syncDir/$chglogDataFile $syncDir/$chglogDataFile.$lastChangeProcessed`;
            }

            #Get all the changes from the last change we processed to the last change made on source.
            $logger->debug("SCRIPT|Getting changes from $processCN");
`$ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort -b "cn=changelog" -S "changenumber" "(changenumber>=$processCN)" > $syncDir/$chglogDataFile`;
            $logger->debug(
"SCRIPT|get change log data ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort -b 'cn=changelog' -S 'changenumber' '(changenumber>=$processCN)' "
            );
        }

        #start here if process an already prepared chglog file.
        unless ($applyldif) {
            my $recordCount = `wc -l $syncDir/$chglogDataFile`;
            $logger->info("SCRIPT|Size of change log file $recordCount lines");
            if ( $recordCount == 0 ) {
                $logger->debug("SCRIPT|Saving last change number $processCN to  $lcnFile ");
                `echo $processCN > $lcnFile`;    #save the last change number processed
                $processCN++;
            } else {
                open( CHG, "< $syncDir/$chglogDataFile" );

                #process the Change log as per business rules
                while (<CHG>) {
                    $logger->debug("SCRIPT|Processing changes at line $_");
                    chomp;
                    if ( ( $_ =~ /changenumber=/ ) && ( $_ =~ /cn=changelog/ ) ) {
                        @a = split( ",", $_ );
                        @b = split( "=", $a[0] );
                        $chgnumber = $b[1];
                        $logger->debug("SCRIPT|Change number in change log $chgnumber");
                        while (<CHG>) {
                            chomp;
                            if ( $_ =~ /targetdn=/ ) {
                                $_ =~ s/targetdn/dn/;
                                $_ =~ s/dn=/dn: /;
                                $mydn  = $_;
                                @a     = split( " ", $mydn );
                                @b     = split( ",", $a[1] );
                                $nc    = scalar(@b);
                                $First = $b[0];
                                $base  = $b[1];

                                for ( $i = 2 ; $i < $nc ; $i++ ) {
                                    $base = $base . "," . $b[$i];
                                }
                                $logger->debug("SCRIPT|Writing temporary ldif - $tempDir/curlog-$chgnumber.ldif ");
                                $curlog = "$tempDir/curlog-$chgnumber.ldif";
                                if ( -e "$curlog" ) {
                                    `rm -rf $curlog`;
                                }
                                open( OUT, "> $curlog" );
                                print OUT "$mydn\n";
                                $line = <CHG>;
                                chomp $line;
                                $line =~ s/=/: /;
                                print OUT "$line\n";
                                @a          = split( " ", $line );
                                $changetype = $a[1];
                                $mbx        = 0;
                                $icc        = 1;
                                $idel       = 0;

                                while ( $icc gt 0 ) {

                                    #Special Handling Section
                                    $line = <CHG>;
                                    chomp $line;
                                    if ( $line =~ /::/ ) {

                                        #log any emcoded values
                                        $logger->warn(
                                            "SCRIPT|Encoded value found for $chgnumber at line " . $icc++ . " in $line" );
                                    }
                                    if ( $line =~ /mailboxid/ && $switch_negative eq "true" ) {
                                        if ( $line =~ /-/ ) {
                                            $line =~ s/-//;

                                            #remove any negative mailbox ids
                                            $logger->debug("SCRIPT|Inverted negative number");
                                        }
                                    }
                                    if ( $line =~ /mailpasswordtype/ ) {
                                        if ( $line =~ /2/ ) {
                                            $line =~ s/2/S/;
                                            $logger->debug("SCRIPT|Custom to SSHA");
                                        }
                                    }
                                    if ( $line =~ /netmailmsgviewprivacyfilter/ ) {
                                        $line =~ s/true/alwaysBlock/i;
                                        $line =~ s/false/alwaysAllow/i;
                                        $logger->debug("SCRIPT|Handling privacy filter");
                                    }
                                    if ( $line =~ /changenumber/ ) {
                                        $icc = 0;
                                    } else {
                                        if ( $line =~ /adminpolicydn: / ) {

#The Class of Service or Admin Policy in the chglog is a primary key reference (entryid). Get the COS name from the destination box and check that it exists.
#If the COS does not exist ignore this change
#The COS and entry id are loaded into a in memory hash to avoid going back to the server every time.
                                            my $adminpolicy;
                                            @a = split( " ", $line );
                                            $entryid = $a[1];
                                            if ( $useDefaultCos eq "true" ) {

                                                #Set all COSs to the default as per the migration.conf regardless.
                                                $line =~ s/$entryid/cn=$defaultCos,cn=admin root/;
                                            } else {
                                                if ( defined( $entryidCache{$entryid} ) ) {
                                                    $adminpolicy = $entryidCache{$entryid};
                                                } else {
                                                    @a =
`$ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort "entryid=$entryid" dn`;
                                                    $na = scalar(@a);
                                                    if ( $na == 0 ) {

                                                  #reject the change entry if the COS does not exist on the destination platform
                                                        $logger->info("SCRIPT|REJECT -- EntryID NOT FOUND -- $line\n");
                                                        $logger->info("SCRIPT|REJECT -- Change Log Skipped -- $curlog\n");
                                                        $idel = 1;
                                                    }
                                                    $adminpolicy = $a[0];
                                                    chomp $adminpolicy;
                                                    $entryidCache{$entryid} = $adminpolicy;
                                                }
                                                $line =~ s/$entryid/$adminpolicy/;
                                            }
                                        } elsif ( $line =~ /adminpolicygrantdn: / ) {
                                            my $adminpolicygrant;
                                            my $adminpolicygrant;
                                            @a = split( " ", $line );
                                            $entryid = $a[1];
                                            if ( $useDefaultCos = "true" ) {

                                                #Set all COSs to the default as per the migration.conf regardless.
                                                $line =~ s/$entryid/cn=$defaultCos,cn=admin root/;
                                            } else {
                                                if ( defined( $entryidCache{$entryid} ) ) {
                                                    $adminpolicygrant = $entryidCache{$entryid};
                                                } else {
                                                    @a =
`$ldapsearch -D cn=root -w $srcMasterPass-h $srcMasterHost -p $srcMasterPort "entryid=$entryid" dn`;
                                                    $na = scalar(@a);
                                                    if ( $na == 0 ) {
                                                        $logger->info("SCRIPT|REJECT -- EntryID NOT FOUND -- $line\n");
                                                        $logger->info("SCRIPT|REJECT -- Change Log Skipped -- $curlog\n");
                                                        $idel = 1;
                                                    }
                                                    $adminpolicygrant = $a[0];
                                                    chomp $adminpolicygrant;
                                                    $entryidCache{$entryid} = $adminpolicygrant;
                                                }
                                                $line =~ s/$entryid/$adminpolicygrant/;
                                            }
                                        } elsif ( $line =~ /admintargetdn: / ) {
                                            my $admintarget;
                                            @a = split( " ", $line );
                                            $entryid = $a[1];
                                            if ( defined( $entryidCache{$entryid} ) ) {
                                                $admintarget = $entryidCache{$entryid};
                                            } else {
                                                @a =
`$ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort "entryid=$entryid" dn`;
                                                $na = scalar(@a);
                                                if ( $na == 0 ) {
                                                    $logger->info("SCRIPT|REJECT -- EntryID NOT FOUND -- $line\n");
                                                    $logger->info("SCRIPT|REJECT -- Change Log Skipped -- $curlog\n");
                                                    $idel = 1;
                                                }
                                                $admintarget = $a[0];
                                                chomp $admintarget;
                                                $entryidCache{$entryid} = $admintarget;
                                            }
                                            $line =~ s/$entryid/$admintarget/;
                                        } elsif ( $line =~ /adminallowedadminpolicydn: / ) {
                                            my $adminallowedadminpolicydn;
                                            @a = split( " ", $line );
                                            $entryid = $a[1];
                                            if ( defined( $entryidCache{$entryid} ) ) {
                                                $adminallowedadminpolicydn = $entryidCache{$entryid};
                                            } else {
                                                @a =
`$ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort "entryid=$entryid" dn`;
                                                $na = scalar(@a);
                                                if ( $na == 0 ) {
                                                    $logger->info("SCRIPT|REJECT -- EntryID NOT FOUND -- $line\n");
                                                    $logger->info("SCRIPT|REJECT -- Change Log Skipped -- $curlog\n");
                                                    $idel = 1;
                                                }
                                                $adminallowedadminpolicydn = $a[0];
                                                chomp $adminallowedadminpolicydn;
                                                $entryidCache{$entryid} = $adminallowedadminpolicydn;
                                            }
                                            $line =~ s/$entryid/$adminallowedadminpolicydn/;
                                        } elsif ( $line =~ /adminroledn: / ) {
                                            my $adminrole;
                                            @a = split( " ", $line );
                                            $entryid = $a[1];
                                            if ( defined( $entryidCache{$entryid} ) ) {
                                                $adminrole = $entryidCache{$entryid};
                                            } else {
                                                @a =
`$ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort "entryid=$entryid" dn`;
                                                $na = scalar(@a);
                                                if ( $na == 0 ) {
                                                    $logger->info("SCRIPT|REJECT -- EntryID NOT FOUND -- $line\n");
                                                    $logger->info("SCRIPT|REJECT -- Change Log Skipped -- $curlog\n");
                                                    $idel = 1;
                                                }
                                                $adminrole = $a[0];
                                                chomp $adminrole;
                                                $entryidCache{$entryid} = $adminrole;
                                            }
                                            $line =~ s/$entryid/$adminrole/;
                                        } elsif ( $line =~ /adminrealmdn: / ) {
                                            my $adminrealm;
                                            @a = split( " ", $line );
                                            $entryid = $a[1];
                                            if ( defined( $entryidCache{$entryid} ) ) {
                                                $adminrealm = $entryidCache{$entryid};
                                            } else {
                                                @a =
`$ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort "entryid=$entryid" dn`;
                                                $na = scalar(@a);
                                                if ( $na == 0 ) {
                                                    $logger->info("SCRIPT|REJECT -- EntryID NOT FOUND -- $line\n");
                                                    $logger->info("SCRIPT|REJECT -- Change Log Skipped -- $curlog\n");
                                                    $idel = 1;
                                                }
                                                $adminrealm = $a[0];
                                                chomp $adminrealm;
                                                $entryidCache{$entryid} = $adminrealm;
                                            }
                                            $line =~ s/$entryid/$adminrealm/;
                                        } elsif ( $line =~ /member: / ) {
                                            my $member;
                                            @a = split( " ", $line );
                                            $entryid = $a[1];
                                            if ( defined( $entryidCache{$entryid} ) ) {
                                                $member = $entryidCache{$entryid};
                                            } else {
                                                @a =
`$ldapsearch -D cn=root -w $srcMasterPass -h $srcMasterHost -p $srcMasterPort "entryid=$entryid" dn`;
                                                $na = scalar(@a);
                                                if ( $na == 0 ) {
                                                    $logger->info("SCRIPT|REJECT -- EntryID NOT FOUND -- $line\n");
                                                    $logger->info("SCRIPT|REJECT -- Change Log Skipped -- $curlog\n");
                                                    $idel = 1;
                                                }
                                                $member = $a[0];
                                                chomp $member;
                                                $entryidCache{$entryid} = $member;
                                            }
                                            $line =~ s/$entryid/$member/;
                                        } elsif ( $line =~ /mailsmtprelayhost/ ) {
                                            $idel = 1;
                                            $logger->info("SCRIPT|REJECT -- $mydn -- REJECT\n");
                                            $logger->info("SCRIPT|REASON: PROXY DATA -- REJECT\n");
                                        } elsif ( $line =~ /mailpopproxyhost/ ) {
                                            $idel = 1;
                                            $logger->info("SCRIPT|REJECT -- $mydn -- REJECT\n");
                                            $logger->info("SCRIPT|REASON: PROXY DATA -- REJECT\n");
                                        } elsif ( $line =~ /mailimapproxyhost/ ) {
                                            $idel = 1;
                                            $logger->info("SCRIPT|REJECT -- $mydn -- REJECT\n");
                                            $logger->info("SCRIPT|REASON: PROXY DATA -- REJECT\n");
                                        } elsif ( $line =~ /$probeDN/ && $ignoreProbe eq "true" ) {
                                            $idel = 1;
                                            $logger->info("SCRIPT|REJECT -- $mydn -- REJECT\n");
                                            $logger->info("SCRIPT|REASON: Probe do not sync -- REJECT\n");
                                        }
                                        print OUT "$line\n";
                                    }
                                    if ( $line =~ /mailboxstatus:/ ) {
                                        unless ( $line =~ /old/ ) {
                                            @a         = split( " ", $line );
                                            $oldstatus = $a[1];
                                            $mbx       = 1;
                                            if ( $oldstatus eq "P" ) {
                                                $idel = 1;
                                                $logger->info("SCRIPT|REJECT -- $mydn -- REJECT\n");
                                                $logger->info("SCRIPT|REASON: PROXY DATA -- REJECT\n");
                                            }
                                        }
                                    }
                                }

                                $jx = 1;
                                close OUT;
                                if ( -e "$curlog.orig" ) {
                                    `rm -rf $curlog.orig`;
                                }
                                if ( $idel == 1 ) {
                                    `rm -rf $curlog`;
                                    next;
                                }
                                `cp $curlog $curlog.orig`;
                                `sed -i '/changes=replace: modifytimestamp/,+5d' $curlog`;
                                `sed -i '/modifytimestamp:/d' $curlog`;
                                `sed -i '/createtimestamp:/d' $curlog`;
                                `sed -i '/modifiersname:/d' $curlog`;
                                `sed -i '/creatorsname:/d' $curlog`;
                                `sed -i '/entryid:/d' $curlog`;
                                `sed -i 's/changes=//' $curlog`;
                                `sed -i 's/newrdn=/newrdn: /' $curlog`;
                                `sed -i 's/newsuperior=/newsuperior: /' $curlog`;
                                `sed -i 's/deleteoldrdn=/deleteoldrdn: /' $curlog`;
                                `sed -i '/mailrealm:/d' $curlog`;

                                if ($mailsetblock) {
                                    `sed -i '/replace: mailautoreplyhost/,+2d' $curlog`;
                                    `sed -i '/replace: mailmessagestore/,+2d' $curlog`;
                                }
                                if ( $mydn =~ /mail=/ ) {
                                    if ( $mbx eq "1" ) {

         #Get the current mailboxstatus on the destination box
         #$status = existing status on destination
         #$oldstatus = is the 'new' status in the source change log
         #$newstatus = is the new status for the destination change log
         #When the change log is updating the status to M or P - do not port this change, leave the destination status unchanged
         #When the destination mailbox status is P , do not change it at all in any circumstances.
                                        @a =
`$ldapsearch -D cn=root -w $destMasterPass -h $destMasterHost -p $destMasterPort -b "$base" "$First" mailboxstatus`;
                                        $nc = scalar(@a);
                                        if ( $nc == 2 ) {
                                            $mailboxstatus = $a[1];
                                            chomp $mailboxstatus;
                                            @a = split( "=", $mailboxstatus );
                                            $status = $a[1];
                                            chomp $status;
                                            if ( $oldstatus eq "M" ) {
                                                $newstatus = $status;
                                            } elsif ( $oldstatus eq "P" ) {
                                                $newstatus = $status;
                                            } else {
                                                $newstatus = $oldstatus;
                                            }
                                            if ( $status eq "P" ) {
                                                $newstatus = $status;
                                            }
                                            if ( $oldstatus ne $newstatus ) {
                                                open( CLG, "< $curlog" );
                                                open( CGL, "> $curlog.tmp" );
                                                while (<CLG>) {
                                                    chomp;
                                                    if ( $_ =~ /mailboxstatus:/ ) {
                                                        unless ( $_ =~ "oldmailboxstatus" ) {
                                                            $_ = "mailboxstatus: $newstatus";
                                                        }
                                                    }
                                                    print CGL "$_\n";
                                                }
                                                close CGL;
                                                close CLG;
                                                `mv $curlog.tmp $curlog`;
                                            }
                                        } elsif ($newproxyflag) {

                     #All new accounts shall be created in PROXY mode unless their mailbox status is something other than A or L
                     #Proxy details will be added for each new account
                                            $logger->info(
                                                "SCRIPT| -- Account $First at $base NOT FOUND in $distmaster -- INFO\n");
                                            if ( $oldstatus eq "A" || $oldstatus eq "L" ) {
                                                $logger->info("SCRIPT| -- NEW ACCOUNT WILL BE CREATED IN PROXY MODE -- INFO\n");
                                                $logger->info("SCRIPT| -- ADDING PROXY SETTINGS AND MESSAGE STORE -- INFO\n");
                                                open( CLG, "< $curlog" );
                                                open( CGL, "> $curlog.tmp" );
                                                while (<CLG>) {
                                                    chomp;
                                                    if ( $_ =~ /mailboxstatus:/ ) {
                                                        unless ( $_ =~ "oldmailboxstatus" ) {
                                                            $_ = "mailboxstatus: P";
                                                            print CGL "$_\n";
                                                            print CGL "mailpopproxyhost: $popproxy\n";
                                                            print CGL "mailimapproxyhost: $imapproxy\n";
                                                            print CGL "mailsmtprelayhost: $smtprelay\n";
                                                        }
                                                    } elsif ( $_ =~ /mailmessagestore:/ ) {
                                                        if ( $chgnumber % 2 ) {
                                                            $_ = "mailmessagestore: $mstore1";
                                                            print CGL "$_\n";
                                                        } else {
                                                            $_ = "mailmessagestore: $mstore2";
                                                            print CGL "$_\n";
                                                        }
                                                    } elsif ( $_ =~ /mailautoreplyhost:/ ) {
                                                        if ( $chgnumber % 2 ) {
                                                            $_ = "mailautoreplyhost: $mstore1";
                                                            print CGL "$_\n";
                                                        } else {
                                                            $_ = "mailautoreplyhost: $mstore2";
                                                            print CGL "$_\n";
                                                        }
                                                    } else {
                                                        print CGL "$_\n";
                                                    }
                                                }
                                                close CGL;
                                                close CLG;
                                                `mv $curlog.tmp $curlog`;
                                            }
                                        } else {
                                            $logger->info(
                                                "SCRIPT| -- Acount $First at $base NOT FOUND in $distmaster -- INFO\n");
                                            if ( $oldstatus ne "A" ) {
                                                $logger->info(
"SCRIPT|WARNING -- NEW ACCOUNT IS BEING CREATED WITH MAILBOXSTATUS $oldstatus\n"
                                                );
                                                $logger->info("SCRIPT|WARNING -- $mydn\n");
                                            }
                                        }
                                    }
                                    if ( $changetype eq "add" ) {

                                        #Set the mailmessagestore to the destination host values
                                        $line = `grep mailmessagestore $curlog`;
                                        $ret  = $?;
                                        if ( $ret eq "0" ) {
                                            chomp $line;
                                            @a = split( " ", $line );
                                            $host = $a[1];
                                            if ( !( $host =~ /msgs/ ) ) {
                                                $n = substr $host, -1, 1;
                                                if ( $n % 2 == 1 ) {
                                                    $host9 = $defaultMsgStoreHost;
                                                } else {
                                                    $host9 = $defaultMsgStoreHost;
                                                }
                                                `sed -i "s/$host/$host9/" $curlog`;
                                            }
                                        }
                                    }
                                }
                                last;
                            }
                        }
                        $processCN = $chgnumber;
                        $logger->debug("SCRIPT|Saving last change number $processCN to  $lcnFile ");
                        `echo $processCN > $lcnFile`;    #save the last change number processed
                    }
                }

            }
        }

        #For all successfully created change logs apply them to the target server
        #start here if only processing prepare ldifs
        @a             = `find $tempDir/ -name cur*.ldif`;
        $mychglogfiles = "";
        $act           = scalar(@a);
        if ( $act > 0 ) {
            @a        = sort @a;
            $igood    = 0;
            $iredo    = 0;
            $cnt      = 0;
            $ret      = 0;
            $i34Error = 0;
            for $mod (@a) {
                chomp $mod;
                $mychglogfiles .= " " . $mod;
                $cnt = $cnt + 1;
                `$ldapmodify -D cn=root -w $destMasterPass -h $destMasterHost -p $destMasterPort -v -f $mod > $mod.result 2>&1`;
                $ret = $?;
                my $success = "false";
                my $i34     = "false";
                if ( $ret eq "0" ) {
                    $success = "true";
                } else {
                    my $resultString = `grep -i "return code 0" $mod.result`;
                    if ( length($resultString) > 0 ) {
                        $success      = "true";
                        $resultString = `grep -i "Invalid DN syntax" $mod.result`;
                        if ( $ignoreLdap34 eq "true" && length($resultString) > 0 ) {
                            $success = "true";
                            $i34     = "true";
                        } elsif ( $ignoreLdap34 eq "false" && length($resultString) > 0 ) {
                            $success = "false";
                            $i34     = "true";
                        }
                    }
                }
                if ( $success eq "true" ) {
                    $igood = $igood + 1;
                    `mv $mod $goodDir`;
                    `mv $mod.orig $goodDir`;
                    $chgid = $mod;
                    $chgid =~ s#temp\/curlog-##;
                    $chgid =~ s/\.ldif//;
                    `echo $chgid > $syncDir/lastchangenumber`;    #MB check if required
                    `mv $mod.result $goodDir`;

                    if ( $i34 eq "true" ) {
                        $i34Error++;
                    }
                } else {
                    $iredo = $iredo + 1;
                    `mv $mod $redoDir`;
                    `mv $mod.orig $redoDir`;
                    `mv $mod.result $redoDir`;
                }
            }
            $logger->debug("SCRIPT|Modification files applied are: $mychglogfiles. LCN $sourceLCN");
            $logger->info("SCRIPT|Totals total:$cnt good:$igood redo:$iredo 34Syntax:$i34Error");
            $logger->debug("SCRIPT|Total Modifications = $cnt");
            $logger->debug("SCRIPT|Total Successful Modifications = $igood");
            $logger->debug("SCRIPT|Total Modifications with Problems = $iredo");
            $logger->debug("SCRIPT|Total Modifications with Invalid Syntax = $i34Error");
            $logger->debug("SCRIPT|Successful modifications are in $goodDir directory");
            $logger->debug("SCRIPT|Modifications to redo are in $redoDir directory");
            if ( $skip eq "true" ) { $processCN++ }
        } else {
            $logger->info("SCRIPT|No LDIFS to apply\n");

            if ( $runOnce eq "true" ) {
                $ix = 0;
                $logger->info("SCRIPT|Run once enabled, stopping processing");
            } else {
                sleep($sleepWait) if $ix > 0;
            }
        }
    } else {
        $logger->info("SCRIPT|$processCN not less than $sourceLCN -- changes are up to date\n");

#Write last change processed to file so in the case of a first run where there are no changes to process, the file is still created
        `echo $processCN > $lcnFile`;
        if ( $runOnce eq "true" ) {
            $ix = 0;
            $logger->info("SCRIPT|Run once enabled, stopping processing");
        } else {
            sleep($sleepWait) if $ix > 0;
        }
    }
}
